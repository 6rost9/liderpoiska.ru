(function( $ ) {
    $.fn.contextShow = function() {
		console.log("300 1");
        var $this = this,
            checkBtns = function() {
				$btn = $('[data-button]');
			}
			checkBtns();
			
			$(".nav__list li").first().trigger('click');
			
			$activeBtn = $("nav__list_item--active");
			$nextBtn = $activeBtn.next();
			if (typeof $nextBtn === 'undefined') $nextBtn = $(".nav__list li").first();
			$(".btn--right").click(function() {
				$nextBtn.trigger('click');
			});
			
			$prevBtn = $activeBtn.prev();
			if (typeof $prevBtn === 'undefined') $prevBtn = $(".nav__list li").last();
			$(".btn--left").click(function() {
				$prevBtn.trigger('click');
			});
			
        $btn.click(function(e) {
			$("[data-context]").parent().removeClass("nav__list_item--active");
			e.preventDefault();
			$id = $(this).data('button');
			$thatBtn = $(".nav__list").find('[data-button='+$id+']');
			$thatBtn.parent().addClass("nav__list_item--active");
			
			
			/*
			$nextBtnId = $thatBtn.next().data('button');
			if (typeof $nextBtnId === 'undefined') $nextBtnId = $(".nav__list li").first().data('button');
			$(".btn--right").attr('data-button', $nextBtnId);
			$prevBtnId = $thatBtn.prev().data('button');
			if (typeof $prevBtnId === 'undefined') $prevBtnId = $(".nav__list li").last().data('button');
			$(".btn--left").attr('data-button', $prevBtnId);
			console.log('next -'+$nextBtnId+';  prev - '+$prevBtnId);
			*/
			
			console.log('next -'+$nextBtn+';  prev - '+$prevBtn);
			$data = [$id];
            $context_block = $this.parents(".b-showcase__container").find('[data-context='+$id+']');
			if($context_block.length > 0) {
				$('[data-context]').fadeOut(300);
				$context_block.fadeIn(300);
				checkBtns();
			} else {
				$.ajax({
					url: 'context/load',
					cache: false,
					data: $data,
					success: function(data) {
						var $data = $(data);
						console.log($id);
						$data.hide();
						//$this.before($data);

						$("[data-context]").fadeOut(300, function(){
							//$data.fadeIn(300);
							//$('html,body').animate({scrollTop: $data.offset().top - 20},300);
							$(".b-showcase__content").prepend($data);
							$data.fadeIn(300);
						});
						checkBtns();
					}
				});
				return false;
			}
        });

        return $this;
    };
})(jQuery);
$(document).ready(function(){
	$('[data-context-show]').contextShow();
}); 