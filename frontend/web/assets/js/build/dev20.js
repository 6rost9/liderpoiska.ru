(function( $ ) {
    $.fn.contextShow = function() {
        var $this = this,
            checkBtns = function() {
				//$btn = $('[data-button]');

				$activeBtn = $(".nav__list_item--active");
				$nextBtn = $activeBtn.next();
				if (typeof $nextBtn === 'undefined') $nextBtn = $(".nav__list li").first();
				$(".btn--right").click(function() {
					$nextBtn.trigger('click');
				});

				$prevBtn = $activeBtn.prev();
				if (typeof $prevBtn === 'undefined') $prevBtn = $(".nav__list li").last();
				$(".btn--left").click(function() {
					$prevBtn.trigger('click');
				});
			}
			setTimeout(function() {
				$(".nav__list li").first().trigger('click');
			},100);
			//checkBtns();

		$('.b-showcase__container').on('click', '.btn--right', function(e) {
			$activeBtn = $(".nav__list_item--active");
			$nextBtn = $activeBtn.next();
			if ($nextBtn.length <= 0) {
				$nextBtn = $(".nav__list li").first();
			}
			$nextBtn.trigger('click');
			/*$(".btn--right").click(function() {
				$nextBtn.trigger('click');
			});*/


			/*$(".btn--left").click(function() {
				$prevBtn.trigger('click');
			});*/
		});
		$('.b-showcase__container').on('click', '.btn--left', function(e) {
			$activeBtn = $(".nav__list_item--active");
			$prevBtn = $activeBtn.prev();
			if ($prevBtn.length <= 0) {
				$prevBtn = $(".nav__list li").last();
			}
			$prevBtn.trigger('click');
		});

		$('.b-showcase__container').on('click', '[data-button]', function(e) {
			$("[data-button]").removeClass("nav__list_item--active");
			e.preventDefault();
			id = $(this).data('button');
			$thatBtn = $(".nav__list").find('[data-button='+id+']');
			$thatBtn.addClass("nav__list_item--active");
			$('.b-showcase__container .nav__short select').val(id).change();

		});

		$('.b-showcase__container').on('change', '.nav__short select', function(e) {
//			$("[data-button]").removeClass("nav__list_item--active");
			e.preventDefault();
			$id = $(this).val();
//			$thatBtn = $(".nav__list").find('[data-button='+$id+']');
//			$thatBtn.addClass("nav__list_item--active");


			/*
			$nextBtnId = $thatBtn.next().data('button');
			if (typeof $nextBtnId === 'undefined') $nextBtnId = $(".nav__list li").first().data('button');
			$(".btn--right").attr('data-button', $nextBtnId);
			$prevBtnId = $thatBtn.prev().data('button');
			if (typeof $prevBtnId === 'undefined') $prevBtnId = $(".nav__list li").last().data('button');
			$(".btn--left").attr('data-button', $prevBtnId);
			console.log('next -'+$nextBtnId+';  prev - '+$prevBtnId);
			*/
			console.log($id+'  - id');
			//console.log('next -'+$nextBtn+';  prev - '+$prevBtn);
			//$container = $("[data-context]");
			$context_block = $('[data-context='+$id+']');
			console.log($context_block.data('context')+' - котекст блок');
			if($context_block.length > 0) {
				$('[data-context]').fadeOut(300);
				$context_block.fadeIn(300);
				//checkBtns();
			} else {
				$.ajax({
					url: '/contextload/',
					//cache: false,
					method: 'get',
					data: {'id': $id},
					success: function(data) {
						var $data = $(data);
						console.log($id);
						$data.hide();

						$("[data-context]").fadeOut(300);
						$(".b-showcase__content").prepend($data);
						$data.fadeIn(300);
						//checkBtns();
					}
				});
				return false;
			}
		});
        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.seoTest = function() {
        var $this = this,
            $step = $this.find('[data-step]'),
            $btn = $this.find('[data-btn]'),
            $result = $this.find('[data-sum]'),
            $resultVal = $result.find('span'),
            val = 0,
            click = true,
            faceClass5 = 'b-test__smile--p5',
            faceClass4 = 'b-test__smile--p2',
            faceClass3 = 'b-test__smile--p0',
            faceClass2 = 'b-test__smile--m2',
            faceClass1 = 'b-test__smile--m5',
            faceClassCurrent,
            $testResults = $this.find('[data-results]');

        $step.hide().eq(0).show();

        $testResults.hide();

        faceClassCurrent = faceClass3;

        function checkFace()
        {
            if(val <= -5)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass1);

                faceClassCurrent = faceClass1;
            } else
            if (val <= -2)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass2);

                faceClassCurrent = faceClass2;
            } else
            if (val < 2)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass3);

                faceClassCurrent = faceClass3;
            } else
            if (val < 5)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass4);

                faceClassCurrent = faceClass4;
            } else
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass5);

                faceClassCurrent = faceClass5;
            }

        }

        function showResult(dif, answer)
        {
            for (var $i = 1; $i <= Math.abs(dif); $i++)
            {
                setTimeout(function(){
                    if(dif > 0)
                    {
                        val++;
                        $resultVal.html(val);
                    } else
                    {
                        val--;
                        $resultVal.html(val);
                    }
                    checkFace();
                }, 500*$i);
            }

            setTimeout(function(){
                var $curStep = $step.filter(':visible'),
                    $nextStep = $curStep.next('[data-step]'),
					index = $curStep.index();

				$(".b-test__results").find("table tr").eq(index+1).find(".table__col-3").html(answer);
				$(".b-test__results").find("table tr").eq(index+1).find(".table__col-4").html(dif);
                if($nextStep.next('[data-step]').length == 0)
                {
                    $testResults.slideDown(300);
					if (val >= 5) showRes = 5;
					if (val >= 2 && val < 5) showRes = 2;
					if (val > -2 && val < 2) showRes = 0;
					if (val > -5 && val <= -2) showRes = -2;
					if (val <= -5) showRes = -5;
					$("[data-test-result="+showRes+"]").siblings().hide();
                }

                $curStep.fadeOut(300, function(){
                    $nextStep.fadeIn(300, function(){
                       click = true;
                    });
                });

            }, 1400);

        }

        $btn.click(function(){
            if(click)
            {
                click = false;
                showResult(parseInt($(this).data('btn')), $(this).html());
                $(this).parents('[data-step]').addClass('done');
            }
            return false;
        });
        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.portfolioShow1 = function() {
        var $this = this,
            $btn = $this.find('[data-btn]');
		cnt = 1;
        $btn.click(function(){
            $this.addClass('loading');
            $.ajax({
                url: '/projectload/',
                cache: false,
				method: 'get',
				data: {'id' : cnt},
                //dataType: "json",
                success: function(data) {
                    var $data = $(data.html);

                    $data.hide();
                    $this.before($data);

                    $this.fadeOut(300, function(){
                        $data.fadeIn(300);
                        $('html,body').animate({scrollTop: $data.offset().top - 20},300);
                    });
					var ending = '';
					if (data.num > 1 && data.num < 5) ending = 'a';
					else if (data.num > 4) ending = 'ов';
					if (data.num > 0) {
						$this.removeClass('loading').fadeIn(300).find('a').html('Показать еще '+data.num+' проект'+ending);
					}
					cnt++;
                }
            });
            return false;
        });

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.orderSend = function() {
        var $this = this;
		$this.find('input[name=phone]').inputmask("+9 (999) 999-99-99");  //static mask
        $btn = $this.find('.btn-submit');
        $btn.click(function(){
            //$this.addClass('loading');
            $.ajax({
				beforeSend: function() {
					$(".popup_bg").fadeIn(300);
					$(".popup_bg").addClass('loading');
				},
                url: '/order/new/',
                cache: false,
				method: 'post',
				data: $this.serialize(),
                //dataType: "json",
                success: function(data) {
					var $data = $(data);
					// $data.hide();
					// $this.before($data);
					$(".popup").fadeIn(300).find("p").html(data.msg);
					$(".popup_bg").removeClass('loading');
					if (data.completed == 1) {
						$this[0].reset();
						$(".popup").removeClass("err");
					} else if (data.completed == 0) {
						$(".popup").addClass("err");
					}
                }
            });
            return false;
        });

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.mapContacts = function() {
        if(this.length > 0)
        {
            var $this = this,
                $list = $this.find('.list'),
                $listLinks = $list.find('.list__link'),
                $text = $this.find('.text'),
                myMap,
				curRegion = $('[data-geo-region]').data('geo-region');
				$('[data-geo-tel='+curRegion+']').siblings().hide();

			if (curRegion == 'other')
				curRegion = 'samara';

            function showPoint(link)
            {
                var $data = link.data('adress');

                link.parent('li').addClass('list__item--active').siblings('li').removeClass('list__item--active');
                $text.filter('[data-adress="' + $data + '"]').show().siblings('.text').hide();

            }

            function init () {
                var pointDestination = {
                        moscow:[55.70414877739966,37.61585649999999],
						samara:[53.20333279, 50.14520750]
                    },
                    myMap = new ymaps.Map('map', {
                        center: pointDestination[curRegion],
                        zoom: 14,
                        controls: ['smallMapDefaultSet']
                    }),
                    pointOptions = {
                        iconLayout: 'default#image',
                        iconImageHref: '/assets/img/icon-map.png',
                        iconImageSize: [23, 32],
                        iconImageOffset: [-11, -32]
                    },
                    pointMoscow = new ymaps.Placemark(pointDestination['moscow'], null, pointOptions),
                    pointSamara = new ymaps.Placemark(pointDestination['samara'], null, pointOptions);


                myMap.geoObjects.add(pointMoscow).add(pointSamara);

                $listLinks.click(function(){
                    if(!$(this).parent('li').hasClass('list__item--active'))
                    {
                        showPoint($(this));
                        myMap.setCenter(pointDestination[$(this).data('adress')], 14);
                    }
                });

            }

            ymaps.ready(init);

            showPoint($('[data-adress='+curRegion+']'));
        }

        return this;
    };
})(jQuery);

$(document).ready(function(){

  // var containerEl = document.querySelector('.b-sites__wrap');
  // var mixer = mixitup(containerEl).use;

  // var showBtn = function (more) {
  //   $('.b-sites__showmore').show();
  //   $('.b-sites__showmore').find('span').html(more)
  // }
  //
  // var hideBtn = function () {
  //   $('.b-sites__showmore').hide();
  // }
  //
  // var projectCount = function () {
  //   cnt = 0;
  //   $('.b-sites__wrap').find('.b-sites__item').each(function () {
  //     if ($(this).css('display') != 'none') {
  //       cnt++;
  //     }
  //   })
  //   return cnt;
  // }
  // var ptList = $('.project_types');
  // var pjList = $('.b-sites__wrap').find('.b-sites__item');
  //
  // function setDelay(index, elem) {
  //   setTimeout(function() {
	// 			$(elem).fadeIn();
  //   }, 20*index);
	// }

  // ptList.on('click', 'li', function () {
  //   var elems = [];
  //   // console.log($(this).data('filter'));
  //   if ($(this).data('filter') == 'all') {
  //     pjList.each(function () {
  //       $(this).fadeIn();
  //     })
  //   } else {
  //     var fparam = $(this).data('filter');
  //     pjList.each(function (index) {
  //       if ($(this).hasClass(fparam) && $(this).css('display') == 'none' ) {
  //         // $(this).fadeIn();
  //         setDelay(index, this);
  //         elems.push(this);
  //       } else if ($(this).hasClass(fparam) && $(this).css('display') != 'none') {
  //
  //       } else {
  //         $(this).hide();
  //       }
  //     });
  //     // pjList.each(function () {
  //     //   $(this).hide();
  //     // })
  //   }
  //
  //
  //
  //     // for (var i = 0; i < pjList.length; i++) {
  //     //   // if ($(pjList).indexOf($(elems).eq(i)) != -1) {
  //     //   //     // $(elems).eq(i).fadeIn(100, function() {
  //     //   //     //     $(this).delay(20);
  //     //   //     console.log(this);
  //     //   //     // });
  //     //   //     i = ++i
  //     //   //   }
  //     //   // pjList.includes($(elems).eq(i));
  //     //   console.log(elems.eq(i));
  //     //   ++i
  //     // }
  //
  //     // var len = pjList.length;
  //     // var i = 0;
  //     // ! function go() {
  //     //   if (elems.indexOf($(elems).eq(i)) != -1) {
  //     //     // $(elems).eq(i).fadeIn(100, function() {
  //     //     //     $(this).delay(20);
  //     //     console.log(this);
  //     //         go();
  //     //
  //     //     // });
  //     //     i = ++i
  //     //   } else {
  //     //     // $(elems).eq(i).fadeOut(100, function() {
  //     //     //     $(this).delay(20);
  //     //         go();
  //     //     // });
  //     //
  //     //     i = ++i
  //     //   }
  //  //
  //  // }()
  //
  // })

  // $('.project_types').on('click', 'li', function () {
  //   projectCount();
  // })

 //  $('.b-sites__wrap').on('mixEnd', function() {
 //    console.log(projectCount());
 //    if (projectCount() >= 5 ) {
 //      showBtn(projectCount()-5);
 //
 //    } else {
 //      hideBtn();
 //    }
 // });



  // $('.b-sites__showmore').on('click', function () {
  //
  // })
  //
  // var filterClick = function () {
  //   $('.b-sites__wrap').find('.b-sites__item').each(function (index) {
  //     if (index > 5) {
  //       $(this).css('display', 'none');
  //     }
  //   })
  // }

  // projectCount();

  // var projects =

  // console.log(projects);

  // if ($(window).width() <= 2000) {
  //   if (projectCount() <=5 && projectCount() != 0 ) {
  //     showBtn(projectCount());
  //   } else if (projectCount() != 0) {
  //     showBtn(5);
  //   }
  //   filterClick();
  // }

  // $(window).on('resize', function () {
  //   if ($(window).width() <= 560) {
  //     showBtn();
  //   } else {
  //     hideBtn();
  //   }
  // })


  // external js: isotope.pkgd.js

  $(document).ready(function() {

    // init Isotope
    var $container = $('.isotope').isotope({
      itemSelector: '.element-item',
      layoutMode: 'fitRows',
      getSortData: {
        name: '.name',
        symbol: '.symbol',
        number: '.number parseInt',
        category: '[data-category]',
        // weight: function(itemElem) {
        //   var weight = $(itemElem).find('.weight').text();
        //   return parseFloat(weight.replace(/[\(\)]/g, ''));
        // }
      }
    });
    function declOfNum(number, titles)
    {
        cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }


    // filter functions
    // var filterFns = {
    //   // show if number is greater than 50
    //   numberGreaterThan50: function() {
    //     var number = $(this).find('.number').text();
    //     return parseInt(number, 10) > 50;
    //   },
    //   // show if name ends with -ium
    //   ium: function() {
    //     var name = $(this).find('.name').text();
    //     return name.match(/ium$/);
    //   }
    // };

    // bind filter button click
    $('#filters').on('click', 'li', function() {
      var filterValue = $(this).attr('data-filter');
      // use filterFn if matches value
      // filterValue = filterFns[filterValue] || filterValue;
      $container.isotope({
        filter: filterValue
      });
    });

    $('#filters_mobile').on('change', function () {
      var filterValue = $(this).find('option:selected').attr('data-filter');
      $container.isotope({
        filter: filterValue
      });
    })

    // bind sort button click
    // $('#sorts').on('click', 'li', function() {
    //   var sortByValue = $(this).attr('data-sort-by');
    //   $container.isotope({
    //     sortBy: sortByValue
    //   });
    // });

    // change is-checked class on buttons
    $('.button-group').each(function(i, buttonGroup) {
      var $buttonGroup = $(buttonGroup);
      $buttonGroup.on('click', 'li', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');
      });
    });

    //****************************
    // Isotope Load more button
    //****************************
    if ($(window).width() <= 560) {
      var initShow = 5; //number of items loaded on init & onclick load more button
      var counter = initShow; //counter for load more button
      var iso = $container.data('isotope'); // get Isotope instance

      loadMore(initShow); //execute function onload

      function loadMore(toShow) {
        $container.find(".hidden").removeClass("hidden");
        var itemsLeft = iso.filteredItems.length - toShow;
        var hiddenElems = iso.filteredItems.slice(toShow, iso.filteredItems.length).map(function(item) {
          return item.element;
        });
        $(hiddenElems).addClass('hidden');
        $container.isotope('layout');

        //when no more to load, hide show more button
        if (hiddenElems.length == 0) {
          jQuery("#load-more").hide();
        } else {
          if (itemsLeft < 5) {
            jQuery("#load-more span").html(itemsLeft+' '+declOfNum(itemsLeft, ['работу', 'работы', 'работ']));
          } else {
            jQuery("#load-more span").html('5 работ');
          }
          jQuery("#load-more").show();
        };

      }

      //append load more button
      if (iso.filteredItems.length < 5 ) {
        $container.after('<div id="load-more"><p>Показать еще <span>'+iso.filteredItems.length+' '+declOfNum(iso.filteredItems.length, ['работу', 'работы', 'работ'])+'</span></p></div>');
      } else {
        $container.after('<div id="load-more"><p>Показать еще <span>5 работ</span></p></div>');
      }


      //when load more button clicked
      $("#load-more").click(function() {
        if ($('#filters').data('clicked')) {
          //when filter button clicked, set initial value for counter
          counter = initShow;
          $('#filters').data('clicked', false);
        } else {
          counter = counter;
        };

        counter = counter + initShow;

        loadMore(counter);
      });

      //when filter button clicked
      $("#filters").click(function() {
        $(this).data('clicked', true);

        loadMore(initShow);
      });

      $('#filters_mobile').on('change', function () {
        $(this).data('clicked', true);
        loadMore(initShow);
      })

    }


  });


	$('[data-context-show]').contextShow();
	$('[data-portfolio-show]').portfolioShow1();
	$('[data-test]').seoTest();
	$('.b-order__form').orderSend();
	$("[data-map]").mapContacts()

	$(".popup_bg, button.close").click(function() {
		$(".popup_bg, .popup").fadeOut(300);
	});

	$(".b-feedback__clients .item__link").click(function(e) {
		e.preventDefault();
		$(this).hide();
		$(this).parent().find('.hide_review').css({'display': 'inline'});
		//$(this).parent().find('.x').css('display','inline');
	});

	/*Share props*/
	$(".b-share__link").click(function(e) {
		e.preventDefault();
	});
	shareUrl = window.location.href;
	shareTitle = document.title;
	shareDesc = $('meta[name=description]').attr('content');
	$(".icon-fb").click(function() {
		Share.facebook(shareUrl,shareTitle,'IMG_PATH',shareDesc);
	});
	$(".icon-tw").click(function() {
		Share.twitter(shareUrl,shareTitle);
	});
	$(".icon-vk").click(function() {
		Share.vkontakte(shareUrl,shareTitle,'IMG_PATH',shareDesc);
	});
	$(".link_to_form").click(function(e) {
		e.preventDefault();
		offsettTop = $(".b-order").offset().top;
		$('html, body').animate({'scrollTop': offsettTop}, 500);
		$(".btn-show-form").trigger('click');
	});

	window.blockFotoramaData = true;
});

Share = {
    vkontakte: function(purl, ptitle, pimg, text) {
        url  = 'http://vk.com/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    facebook: function(purl, ptitle, pimg, text) {
        url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&p[url]='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },

    popup: function(url) {
		width = 626;
		height = 436;
        window.open(url,'','toolbar=0,status=0,width='+width+',height='+height+',left=' + ((window.innerWidth - width)/2) + ',top=' + ((window.innerHeight - height)/2));
    }


};
