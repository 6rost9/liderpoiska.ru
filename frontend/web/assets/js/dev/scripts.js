$(document).ready(function(){
	// переключаем прототип <-> дизайн
	$(".scroll_switch > div").on("click", function(){
		if (!$(this).hasClass("_sel")) {
			$block = $(this).parents('section');
			var thumb_top = parseFloat($block.find(".sp-thumb").css("top")).toFixed(2);
			var new_thumb_top = (thumb_top*$block.find(".slide-2 img").height()/$block.find(".slide-1 img").height()).toFixed(2);
			//alert(new_thumb_top);
			var img1_top = $block.find(".slide-1 img").height()*thumb_top/442;
			var img2_top = $block.find(".slide-2 img").height()*new_thumb_top/442;

			$block.find(".scroll_switch > div").removeClass("_sel");
			$(this).addClass("_sel");
			var img1 = $block.find(".slide-1 img").attr("src");
			var img2 = $block.find(".slide-2 img").attr("src");
			$block.find(".slide-1").append("<div class='fantomImg-1'><img alt='' src='"+img1+"' style='margin-top: -"+img1_top+"px' /></div>").find(".sp-viewport").fadeOut(0);
			$block.find(".slide-2").append("<div class='fantomImg-2'><img alt='' src='"+img2+"' style='margin-top: -"+img2_top+"px' /></div>").find(".hide_slide-2").fadeOut(0);
			$block.find(".fantomImg-1").animate({
				"top":100+"px",
				"left":100+"px"
			}).fadeOut(300);
			$block.find(".fantomImg-2").animate({
				"top":-100+"px",
				"left":-100+"px"
			}).fadeOut(200);
			setTimeout(function() {
				$block.find('.fantomImg-1, .fantomImg-2').remove();
				$block.find(".scroll_slider .sp-thumb").css({"top": new_thumb_top + "px"});
			}, 800);
			setTimeout(function() {
				$block.find(".scroll_slider .sp-viewport").fadeIn(300).find("img").attr("src", img2);
				$block.find(".scroll_slider .hide_slide-2").fadeIn(300).find("img").attr("src", img1);
			}, 300);
		}
	});
	/*Табы*/
	if ($('[data-tabs]').length > 0) {
		tab=0;
		$("[data-tabs]").each(function() {
			b = 1;
			$(this).find('[data-tabs-buttons]').children().each(function() {
				$(this).attr('data-tabs-button', b);
				if (b == 1) $(this).addClass('_act');
				b++;
			});
			c = 1;
			$(this).find('[data-tabs-contents]').children().each(function() {
				$(this).attr('data-tabs-content', c);
				if (c == 1) $(this).css({display:'block'});
				c++;
			});
		});
	}
	$('body').on('click', '[data-tabs-button]', function() {
		t = $(this).attr('data-tabs-button');
		$(this).parents('[data-tabs]').find('[data-tabs-content='+t+']').fadeIn().siblings().hide();
		$(this).addClass('_act').siblings().removeClass('_act');
	});
	/*Табы end^^^^^^^*/
	$('.scrollpanel, .swiper_monitor .swiper-slide, .monitor_phone-m, .monitor_phone-p, .swiper_mobile_examples .swiper-slide').scrollpanel();
});
/*(function() {
	var $ = jQuery;
	function init() {

	}
	$(init);
}());*/
swiperNames = function() {
	$('.swiper_names .current').html($('.swiper-slide-active img').attr('alt'));
	$('.swiper_names .prev').html($('.swiper-slide-active').prev().find('img').attr('alt'));
	$('.swiper_names .next').html($('.swiper-slide-active').next().find('img').attr('alt'));
}
var swiper = new Swiper('.swiper-container', {
	pagination: '.swiper-pagination',
	nextButton: '.swiper-button-next',
	prevButton: '.swiper-button-prev',
	slidesPerView: 'auto',
	centeredSlides: true,
	paginationClickable: true,
	spaceBetween: 30,
	loop:true,
	onInit: function(swiper) {
		swiperNames();
    },
	onSlideChangeEnd:  function(swiper) {
		swiperNames();
    },
});

var mySwiper = $('.swiper_mobile_examples').swiper({
	mode: 'horizontal',
	speed: 1000,
	autoplay : 4000,
	pagination: '.swiper-pagination',
	paginationClickable: true,
	centeredSlides: true,
	slidesPerView: 'auto',
	loop: true,
	//scrollbar: '.scrollbar',
	//scrollbarDraggable: true,
	//scrollbarHide: false,
});
var easySwiper = $('.easy_swiper-container').swiper({
	//grabCursor: true,
	mode: 'horizontal',
	speed: 1000,
	autoplay : 4000,
	pagination: '.swiper-pagination',
	paginationClickable: true,
	slidesPerView: 1,
	grabCursor: true,
});
/*
$(function(){
	$('.beforeafter').beforeAfter();
});
*/

$(function() {
	var GammaSettings = {
		viewport : [ {
			width : 1200,
			columns : 5
		}, {
			width : 900,
			columns : 5
		}, {
			width : 500,
			columns : 5
		}, {
			width : 320,
			columns : 2
		}, {
			width : 0,
			columns : 2
		} ]
	};
	Gamma.init( GammaSettings );
});
$(window).load(function() {
	$('.beforeafter').beforeAfter();
	$('.gamma-container').scrollpanel();
	$('.browser_img').each(function() {
		if ($(this).height() > 1500) {
			$(this).scrollpanel();
		}
	});
});
