(function( $ ) {
    $.fn.dataBG = function() {
        return this.each(function(){
            $(this).css({'backgroundImage': 'url(' + $(this).data('bg') + ')'});
        });
    };
})(jQuery);

(function( $ ) {
    $.fn.dataToggleList = function() {
        var $this = this,
            $btn = $this.find('[data-btn-toggle]');

        $btn.click(function(){
            $(this).hide();
            $this.addClass('js-list-visible');
            return false;
        });

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.initFotorama = function() {
        var $this = this,
            $items = $this.find('.item'),
            itemsVal = $items.length,
            $container = $('<div class="fotorama__modal"></div>');

        for(var $i=0; $i < itemsVal; $i++)
        {
            var currItem = $items.eq($i);
            currItem.attr({'data-index': $i});
            $container.append('<img src="' + currItem.attr('href') + '" />');
        }

        $('.b-page').append($container);

        $container.fotorama({
            allowfullscreen: true,
            nav: 'thumbs',
            thumbborderwidth: 0
        });

        var fotorama = $container.data('fotorama');

        $items.click(function(){
            var itemIndex = $(this).data('index'),
                scrolled = $(window).scrollTop();

            fotorama.show(itemIndex);
            fotorama.requestFullScreen();
            $(window).scrollTop(scrolled);

            return false;
        });

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.sliderOpacity = function() {
        var $this = this,
            $itemsHolder = $this.find('[data-items-holder]'),
            $items = $itemsHolder.find('[data-item]'),
            itemsVal = $items.length,
            $nav = $this.find('[data-nav]'),
            pause = 5000,
            timer;

        for(var $i=0; $i < itemsVal; $i++)
        {
            $items.eq($i).attr({'data-index': $i});
            $nav.append('<li class="nav__item"><a data-index="' + $i + '" class="nav__link"></a></li>');
        }

        var $navItems = $nav.find('.nav__item'),
            $navLinks = $nav.find('.nav__link');

        $nav.find('.nav__item').eq(0).addClass('nav__item--active');

        $items.eq(0).addClass('active');

        function nextSlide(index)
        {
            $items.filter('.active').removeClass('active');
            $items.eq(index).addClass('active');
            $navItems.removeClass('nav__item--active').eq(index).addClass('nav__item--active');
        }

        function getNext()
        {
            var $itemActive = $items.filter('.active'),
                $index = $itemActive.next('[data-item]').length ?  $itemActive.next('[data-item]') : $items.eq(0);
            return $index.data('index');
        }

        function autoStart()
        {
            timer = setTimeout(function(){
                var index = getNext();
                nextSlide(index);
            }, pause);
        }

        $navLinks.click(function(){
            var index = $(this).data('index');
            nextSlide(index);
            return false;
        });

        $this.hover(function(){
            clearTimeout(timer);
        }, function(){
            autoStart();
        });

        autoStart();

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.sliderSeo = function() {
        var $this = this;

        $this.each(function(){
            var $this = $(this),
                $itemsHolder = $this.find('[data-items-holder]'),
                $items = $itemsHolder.find('[data-item]'),
                itemsVal = $items.length,
                $btnPrev = $itemsHolder.find('[data-btn-prev]'),
                $btnNext = $itemsHolder.find('[data-btn-next]'),
                $nav = $this.find('[data-nav]'),
                pause = 3000,
                timer,
                click = true,
                animationTime = 600;

            $items.css({'display': 'none'}).eq(0).css({'display': 'block'});

            for(var $i=0; $i < itemsVal; $i++)
            {
                $items.eq($i).attr({'data-index': $i});
                $nav.append('<li class="nav__item"><a data-index="' + $i + '" class="nav__link"></a></li>');
            }

            var $navItems = $nav.find('.nav__item'),
                $navLinks = $nav.find('.nav__link');

            $nav.find('.nav__item').eq(0).addClass('nav__item--active');

            function nextSlide(index)
            {
                click = false;
                $items.filter(':visible').animate({'left': '-100%'},animationTime, function(){
                    $(this).css({'display': 'none'});
                    click = true;
                });
                $items.eq(index).css({'left': '100%', 'display': 'block'}).animate({'left': 0},animationTime);

                $navItems.removeClass('nav__item--active').eq(index).addClass('nav__item--active');
            }

            function prevSlide(index)
            {
                click = false;
                $items.filter(':visible').animate({'left': '100%'},animationTime, function(){
                    $(this).css({'display': 'none'});
                    click = true;
                });
                $items.eq(index).css({'left': '-100%', 'display': 'block'}).animate({'left': 0},animationTime);

                $navItems.removeClass('nav__item--active').eq(index).addClass('nav__item--active');
            }

            function getNext()
            {
                var $itemActive = $items.filter(':visible'),
                    $index = $itemActive.next('[data-item]').length ?  $itemActive.next('[data-item]') : $items.eq(0);
                return $index.data('index');
            }

            function getPrev()
            {
                var $itemActive = $items.filter(':visible'),
                    $index = $itemActive.prev('[data-item]').length ?  $itemActive.prev('[data-item]') : $items.eq(itemsVal - 1);
                return $index.data('index');
            }

            function autoStart()
            {
                timer = setTimeout(function(){
                    var index = getNext();
                    nextSlide(index);
                }, pause);
            }

            $navLinks.click(function(){
                if(click && !$(this).parent('li').hasClass('nav__item--active'))
                {
                    var index = $(this).data('index');
                    nextSlide(index);
                }
                return false;
            });

            $this.hover(function(){
                clearTimeout(timer);
            }, function(){
                //autoStart();
            });

            $btnNext.click(function(){
                if(click)
                {
                    var index = getNext();
                    nextSlide(index);
                }
                return false;
            });

            $btnPrev.click(function(){
                if(click)
                {
                    var index = getPrev();
                    prevSlide(index);
                }
                return false;
            });

            //autoStart();
        });

        $this.hide().eq(0).show();

        var $list = $('.b-seo-results__filter_list'),
            $links = $list.find('.list__link'),
            click = true;

        $links.click(function(){
            if(!$(this).parent('.list__item').hasClass('list__item--active') && click)
            {
                var tab = $(this).data('tab');

                click = false;

                $(this).parent('.list__item').addClass('list__item--active').siblings('.list__item').removeClass('list__item--active');

                $this.filter(':visible').fadeOut(300, function(){
                    $this.filter('[data-slider-seo="' + tab + '"]').fadeIn(300, function(){
                        click = true;
                    });
                });
            }

            return false;
        });

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.bOrder = function() {
        var $this = this,
            $btn = $this.find('[data-btn-show]');

        $btn.click(function(){
            $this.removeClass('b-order--short');
            return false;
        });


        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.mapContacts = function() {
        if(this.length > 0)
        {
            var $this = this,
                $list = $this.find('.list'),
                $listLinks = $list.find('.list__link'),
                $text = $this.find('.text'),
                myMap;

            function showPoint(link)
            {
                var $data = link.data('adress');

                link.parent('li').addClass('list__item--active').siblings('li').removeClass('list__item--active');
                $text.filter('[data-adress="' + $data + '"]').show().siblings('.text').hide();

            }

            function init () {
                var pointDestination = {
                        'moscow': [55.751574, 37.573856],
                        'samara': [53.207032, 50.102144]
                    },
                    myMap = new ymaps.Map('map', {
                        center: pointDestination['samara'],
                        zoom: 9,
                        controls: ['smallMapDefaultSet']
                    }),
                    pointOptions = {
                        iconLayout: 'default#image',
                        iconImageHref: 'img/icon-map.png',
                        iconImageSize: [23, 32],
                        iconImageOffset: [-11, -32]
                    },
                    pointMoscow = new ymaps.Placemark(pointDestination['moscow'], null, pointOptions),
                    pointSamara = new ymaps.Placemark(pointDestination['samara'], null, pointOptions);


                myMap.geoObjects.add(pointMoscow).add(pointSamara);

                $listLinks.click(function(){
                    if(!$(this).parent('li').hasClass('list__item--active'))
                    {
                        showPoint($(this));
                        myMap.setCenter(pointDestination[$(this).data('adress')], 9);
                    }
                });

            }

            ymaps.ready(init);

            showPoint($('[data-adress="samara"]'));
        }

        return this;
    };
})(jQuery);

(function( $ ) {
    $.fn.portfolioShow = function() {
        var $this = this,
            $btn = $this.find('[data-btn]');
		cnt = 1;
        $btn.click(function(){
            $this.addClass('loading');
            $.ajax({
                url: '/project/load',
                cache: false,
				data: cnt,
                dataType: "html",
                success: function(data) {
                    var $data = $(data);

                    $data.hide();
                    $this.before($data);

                    $this.fadeOut(300, function(){
                        $data.fadeIn(300);
                        $('html,body').animate({scrollTop: $data.offset().top - 20},300);
                    });
					cnt++;
                }
            });
            return false;
        });

        return $this;
    };
})(jQuery);

(function( $ ) {
    $.fn.seoTest = function() {
        var $this = this,
            $step = $this.find('[data-step]'),
            $btn = $this.find('[data-btn]'),
            $result = $this.find('[data-sum]'),
            $resultVal = $result.find('span'),
            val = 0,
            click = true,
            faceClass5 = 'b-test__smile--p5',
            faceClass4 = 'b-test__smile--p2',
            faceClass3 = 'b-test__smile--p0',
            faceClass2 = 'b-test__smile--m2',
            faceClass1 = 'b-test__smile--m5',
            faceClassCurrent,
            $testResults = $this.find('[data-results]');

        $step.hide().eq(0).show();

        $testResults.hide();

        faceClassCurrent = faceClass3;

        function checkFace()
        {
            if(val <= -5)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass1);

                faceClassCurrent = faceClass1;
            } else
            if (val <= -2)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass2);

                faceClassCurrent = faceClass2;
            } else
            if (val < 2)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass3);

                faceClassCurrent = faceClass3;
            } else
            if (val < 5)
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass4);

                faceClassCurrent = faceClass4;
            } else
            {
                $result
                    .removeClass(faceClassCurrent)
                    .addClass(faceClass5);

                faceClassCurrent = faceClass5;
            }

        }

        function showResult(dif)
        {
            for (var $i = 1; $i <= Math.abs(dif); $i++)
            {
                setTimeout(function(){
                    if(dif > 0)
                    {
                        val++;
                        $resultVal.html(val);
                    } else
                    {
                        val--;
                        $resultVal.html(val);
                    }
                    checkFace();
                }, 500*$i);
            }

            setTimeout(function(){
                var $curStep = $step.filter(':visible'),
                    $nextStep = $curStep.next('[data-step]');

                if($nextStep.next('[data-step]').length == 0)
                {
                    $testResults.slideDown(300);
                }

                $curStep.fadeOut(300, function(){
                    $nextStep.fadeIn(300, function(){
                       click = true;
                    });
                });

            }, 1400);

        }

        $btn.click(function(){
            if(click)
            {
                click = false;
                showResult(parseInt($(this).data('btn')));
                $(this).parents('[data-step]').addClass('done');
            }
            return false;
        });




        return $this;
    };
})(jQuery);
nav__list_item


(function( $ ) {
    $.fn.contextShow = function() {
		console.log("300");
        var $this = this,
            $btn = $this.find('[data-button]');
			
        $btn.click(function(){
			
			$id = $this.data('button');
			console.log($id);
			
            $context_block = $this.parent().find('[data-context='+$id+']');
			if($context_block.length > 0) {
				$this.parent().find('[data-context]').fadeOut(300);
				$context_block.fadeIn(300);
			} else {
				$.ajax({
					url: 'context/load',
					cache: false,
					data: $id,
					success: function(data) {
						var $data = $(data);

						$data.hide();
						$this.before($data);

						$this.fadeOut(300, function(){
							$data.fadeIn(300);
							//$('html,body').animate({scrollTop: $data.offset().top - 20},300);
						});
					}
				});
				return false;
			}
        });

        return $this;
    };
})(jQuery);


$(document).ready(function(){

    $('[data-bg]').dataBG();
    $('[data-toggle-list]').dataToggleList();
    $('[data-fotorama]').initFotorama();
    $('[dara-slider-opacity]').sliderOpacity();
    $('[ data-slider-seo]').sliderSeo();
    $('[data-show-form]').bOrder();
    $('[data-map]').mapContacts();
    $('[data-portfolio-show]').portfolioShow();
    $('[data-test]').seoTest();
	$('[data-context-show]').contextShow();
	//$('[data-portfolio-show]').portfolioShow();
	

});