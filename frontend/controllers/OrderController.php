<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Order;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

	public function actionNew()
    {
		$msg = '';
		$completed = 0;
		$post = Yii::$app->request->post();
		if ($post['name']!='') {
			$msg .= "You are bot :(";
		} else {
		if (isset($post['name_true']) && $post['name_true']=='') {
			$msg .= "Введите Ваше имя<br>";
		}
		if ($post['url']=='') {
			$msg .= "Введите адрес сайта<br>";
		}
		if ($post['email']=='' && $post['phone']=='') {
			$msg .= "Введите email или телефон<br>";
		} elseif ($post['email']!='' && preg_match("/^.*?@.*?\..*?$/",$post['email'])==0) {
			$msg .= 'Некорректный адрес email<br>';
		}
		$textBody ='';
		if ($msg=='') {
			$model = new Order();
			$model->seo = (!empty($post['seo']) ? 1 : 0);
			$model->conversion = (!empty($post['conversion']) ? 1 : 0);
			$model->context = (!empty($post['context']) ? 1 : 0);
			$model->name = $post['name_true'];
			$model->phone = $post['phone'];
			$model->email = $post['email'];
			$model->url = $post['url'];
			$model->description = $post['description'];
			$model->date = time();
			$save = $model->save();

			$textBody = 'Имя: '.$post['name_true'] ."\r\n";
			$url = (strpos('http', $post['url']) ? $post['url'] : $post['url']);
			$textBody .= 'Адрес сайта: '. $url ."\r\n";
			$textBody .= 'Контакты: '. ($post['phone']!='' ? 'Телефон: '.$post['phone'].' ' : '') . ($post['email']!='' ? 'email: '.$post['email'] : '')."\r\n";
			$textBody .= 'Заявка: '. (!empty($post['seo']) ? "Поисковое продвижение\r\n" : '') . (!empty($post['conversion']) ? "Повышение конверсии\r\n" : '') . (!empty($post['context']) ? "Контекст\r\n" : '');
			$to = ['info@liderpoiska.ru', 'zayavka-site@liderpoiska.planfix.ru'];
			//$to = ['solovyev@liderpoiska.ru', 'krang13@rambler.ru'];
			$textBody .= ($post['description']!='' ? 'Направление деятельности: '. $post['description'] : '')."\r\n";
			$send = Yii::$app->mailer->compose()
                ->setTo($to)
                ->setFrom(['info@liderpoiska.ru' => 'Лидер Поиска'])
                ->setSubject('Заявка от '.$url)
                ->setTextBody($textBody)
                ->send();

			if ($send) {
				$msg = "Заявка отправлена!<br>Специалист свяжется с Вами в течение рабочего дня для уточнения деталей.";
				$completed = 1;
			} else {
				$msg = "Ошибка отправки";
				$completed = 0;
			}
		}
		}
		//header("Content-Type: application/json", true);
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return ['msg' => $msg, 'completed' => $completed];
    }
}
