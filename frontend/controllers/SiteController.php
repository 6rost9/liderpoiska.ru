<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\LoginForm;
use backend\models\ContactForm;
use yii\web\HttpException;

use backend\models\Review;
use backend\models\Project;
use backend\models\Section;
use backend\models\Projectparam;
use backend\models\ProjectTypes;
use backend\models\Param;
use backend\models\Position;
use backend\models\Region;
use backend\models\Page;
use backend\models\Context;
use backend\models\Slider;
use backend\models\Devsection;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\components\IntermediateController;
use backend\models\Order;
use frontend\components\ipgeobase\ipgeobase;

use himiklab\thumbnail\EasyThumbnailImage;

class SiteController extends IntermediateController
{
	//public $layoutParams = 22;

    public $error;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    // тут в beforeAction добавил $action

    public function beforeAction($action) {
        $url_arr = explode("/", Url::to('@web'));
		$reverse = array_reverse($url_arr);
        $aliases = [];
        while (count($reverse) > 0) {
            $aliases[]= array_shift($reverse);
        }

        $alias=$this->alias();
		$alias=='' ? $alias='index' : true;

        $count = count($aliases);

        $page = Page::find()->where(['alias'=>$aliases[1]])->one();
        $i = 2;
        $this->error = false;

        if (!empty($page->parentinfo)) {
            while (!empty($page->parentinfo)) {
                $page = $page->parentinfo;
                if ($page->alias != $aliases[$i]) {
                   $this->error = true;
                } else {

                    $i++;
                }
            }
        } elseif ($count != 3 && $alias != 'index') {
            $this->error = true;
        }



        if (!$this->error) {


            $page = Page::find()->where("alias='$alias'")->one();
                if ($page) {
                    $this->page = $page;
                } else {
                    $this->error = true;
                }

        }
        $this->menu = Page::find()
        ->where("active<>'0' and parent='0' ")
        ->orderBy('`page`.`sort` IS NULL, `page`.`sort` ASC')
        ->all();
		$this->addMenu = Page::find()->where("active<>'0' and parent='2' ")->all();

		//определение региона по ip
		$ip = $_SERVER['REMOTE_ADDR'];//'62.213.6.127';//'95.137.63.255';
		$ipgeobase = new IPGeoBase;
		$this->view->params['geo'] = [];
		//$ip = '178.219.186.12';
		//$ip = '1.1.1.1';
		if (!empty($ipgeobase->getRecord($ip)['city']) && !empty($ipgeobase->getRecord($ip)['region'])) {
			$this->view->params['geo']['city'] = iconv("Windows-1251", "UTF-8", $ipgeobase->getRecord($ip)['city']);
			$this->view->params['geo']['reg'] = iconv("Windows-1251", "UTF-8", $ipgeobase->getRecord($ip)['region']);
		} else {
			$this->view->params['geo']['city'] = '';
			$this->view->params['geo']['reg'] = '';
			$this->view->params['geo']['phone2'] = '';
			$this->view->params['geo']['alias2'] = '';
		}

		if ($this->view->params['geo']['reg'] == 'Самарская область') {
			$this->view->params['geo']['phone'] = '8 (846) 212-97-32';
			$this->view->params['geo']['alias'] = 'samara';
			$this->view->params['geo']['phone2'] = '8 (800) 775-67-49';
			$this->view->params['geo']['alias2'] = 'other';
		} elseif ($this->view->params['geo']['reg'] == 'Москва') {
			$this->view->params['geo']['phone'] = '8 (499) 653-58-95';
			$this->view->params['geo']['alias'] = 'moscow';
			$this->view->params['geo']['phone2'] = '8 (800) 775-67-49';
			$this->view->params['geo']['alias2'] = 'other';
		} else {
			$this->view->params['geo']['phone'] = '8 (499) 653-58-95';
			$this->view->params['geo']['alias'] = 'moscow';
			$this->view->params['geo']['phone2'] = '8 (800) 775-67-49';
			$this->view->params['geo']['alias2'] = 'other';
		}
		$urlArray = explode('?', Url::to('@web'));
		if (count($urlArray) > 1) {
			$this->view->params['canonical'] = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443 ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $urlArray[0];
		}
		
        return true;
    }

    public function actions()
    {



		return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
		$alias="index";

		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();

		$projects = Project::find()
			->where("show_in_slider='1'")
			->joinWith('slider')
			->indexBy('id')
			->orderby('(slider.param + 0)')
			->all();

		$reviews = Review::find()->orderBy('rand()')->with('projects')->all();
		$imgreviews = Review::find()->where("img<>''")->orderBy('rand()')->with('projects')->all();

		$dataProvider = new ActiveDataProvider([
            'query' => Review::find(),
        ]);
		$projectparams = new ActiveDataProvider([
            'query' => Project::find()->joinWith([
				'projectparams' => function ($query) {
					$query->indexBy('alias');
				},
			'slider'])//->all()//->indexBy('alias'),
        ]);
		//echo "<pre>";print_r($projects);exit;

        return $this->render('index.twig', ['dataProvider' 	=>	$dataProvider,
											'projectparams'	=>	$projectparams,
											'projects'		=>	$projects,
											'sec'			=>	$sec,
											'reviews' 		=>	$reviews,
											'imgreviews'	=>	$imgreviews,
											]);
    }

    public function actionDefault ()
    {
      $url = explode("/", Url::to('@web'));
      $alias = end($url);
      return $this->render($this->alias().'.twig', [	'sec' => $sec, ]);
    }

	public function actionSeo()
    {
		/*
		//перенесено в beforeAction
		$ip = $_SERVER['REMOTE_ADDR'];//'62.213.6.127';//'95.137.63.255';
		$ipgeobase = new IPGeoBase;
		if (!empty($ipgeobase->getRecord($ip)['city']) && !empty($ipgeobase->getRecord($ip)['region'])) {
			$geo_city = iconv("Windows-1251", "UTF-8", $ipgeobase->getRecord($ip)['city']);
			$geo_reg = iconv("Windows-1251", "UTF-8", $ipgeobase->getRecord($ip)['region']);
		} else {
			$geo_city = '';
			$geo_reg = '';
		}*/
		$geo_city = $this->view->params['geo']['city'];
		$geo_reg = $this->view->params['geo']['reg'];

		$sec = Section::find()->where("parent='".$this->alias()."'")->with(['sections'=>function($query) {
			// $query->orderBy('position');
		// }])->all();
    }])->orderBy(['position'=>'ASC'])->all();

    // echo "<pre>";
    // print_r($sec);
    // echo "</pre>";

		$region = Region::find()->joinWith(['projects'=>function($query) {
			$query->joinWith(['positions'=>function($q) {
				$q->where('active=1')->where('weight>0')->orderBy('weight DESC')->limit(1000000);
			}])->limit(1000);
		}])->having("count(project.region)>0")->groupBy('region.id')->orderBy('show_in_slider DESC, sorting')->all();

		//echo "<pre>"; var_dump($ipgeobase->getRecord($ip)); print_r($geo_reg); exit;
		$need_del = false;
		foreach ($region as $k =>$r) {
			if ($r->name == $geo_city || $r->region_full == $geo_reg) {
				$need_del = $k;
				break;
			}
		}
		if ($need_del) {
			$del = array_splice($region, $need_del, 1);
			array_unshift($region, $del[0]);
		}
//		print_r($region);
//		exit;
/*
		$pos = Project::find()->with(['positions'=>function($q) {
			$q->where('weight>0')->orderBy('weight DESC')->limit(50000);
		}])->all();
*/
		$reviews = Review::find()->orderBy('rand()')->with('projects')->all();
		$imgreviews = Review::find()->where("img<>''")->orderBy('rand()')->with('projects')->all();

		$order = new Order();
		
		$projects = Project::find()->where("show_in_slider='1'")->joinWith([
			'slider'=>function($q) {
				$q->orderby('(param + 0)');
			}
		])->indexBy('id')->all();//->all()//->indexBy('alias'),
        return $this->render($this->alias().'.twig', [	'sec'			=>	$sec,
										//	'pos'			=>	$pos,
											'region'		=>	$region,
											'imgreviews'	=>	$imgreviews,
											'reviews'	=>	$reviews,
											'projects'	=>	$projects,
											'order'	=>	$order,]);
    }

	public function actionYandex()
    {
		//$url=end(explode("/", Url::to('@web')));

    // echo $this->alias(); exit;



		$sec = Section::find()->where("parent='".$this->alias()."'")->with('sections')->all();

		/*echo "<pre>";
		print_r($region);
		echo "</pre>";*/

        return $this->render($this->alias().'.twig', ['sec' => $sec]);
    }

	public function alias() {
		$url=explode("/", Url::to('@web'));
		$cnt = count($url)-2;
		$alias = $url[$cnt];
		return $alias;
	}

	public function actionCanpromote()
    {
		$sec = Section::find()->where("parent='".$this->alias()."'")->with('sections')->all();

		//$alias=array_pop(explode("/",$url));
		//echo "<pre>";
		//print_r($region);
		//echo "</pre>";

		//$page = Page::find()->where('parent="/seo"')->with('sections')->all();

		return $this->render($this->alias().'.twig', ['sec' => $sec]);

        //return $this->render('yandex.twig', ['sec' => $sec]);
    }
	public function actionPortfolio()
    {
		$sec = Section::find()->where("parent='".$this->alias()."'")->with('sections')->all();

	/*	$region = Region::find()->with(['projects'=>function($query) {
			$query->with(['positions'=>function($q) {
				$q->where('active=1')->where('position<11')->orderBy('(1/position*frequency) DESC')->limit(1000);
			}])->limit(100);
		}])->orderBy('show_in_slider DESC, id')->all();
	*/
		$project = Project::find()->where('active=1')->joinWith(['positions'=>function($q) {
			$q->where('weight>0')->orderBy('weight DESC');
		}])->with(['regions'=>function($query) {}])->andWhere(['>','position.weight', '0'])->groupBy('project.id')->limit(20)->all();
/*
		$project = Project::find()->select('project.*, position.*, count(position.id)')
		->leftJoin('position', '`position`.`project_id`=`project`.`id`')
//		->join('LEFT JOIN', 'region', 'region.id=project.region')
		//	->with(['positions'=>function($q) {
		//		$q->where('weight>0')->orderBy('weight DESC');
		//	}])
		->where(['project.active'=>'1'])
		->andWhere(['>','position.weight', '0'])
		->limit(1000)
		->orderBy('project.id, weight DESC')
		->groupBy('project.id')
		->asArray()->all();
		echo "<pre>";
		print_r($project);exit;
*/
		$cnt=count($project);
		$num = (($cnt-10) > 0) ? $cnt-10 : 0;
		array_splice($project,10);
		$reviews = Review::find()->orderBy('rand()')->with('projects')->all();
		$imgreviews = Review::find()->where("img<>''")->orderBy('rand()')->with('projects')->all();

		return $this->render($this->alias().'.twig', [	'sec' 		=> 	$sec,
												'project'	=>	$project,
												'imgreviews'=>	$imgreviews,
												'reviews'	=>	$reviews,
												'num'		=>	$num]);
    }



	// public function actionContext()
  //   {
	// 	$alias=end(explode("/", Url::to('@web')));
	// 	$sec = Section::find()->where("parent='".$this->alias()."'")->with('sections')->orderBy('position')->all();
  //
	// 	$context = Context::find()->with('projects')->all();
  //
	// 	//echo "<pre>";
	// 	//print_r($context);
	// 	//echo "</pre>";
  //
	// 	return $this->render($this->alias().'.twig', [	'sec' => $sec,
	// 											'context' => $context,
	// 											]);
  //   }

	public function actionConversion()
    {
      $alias=end(explode("/", Url::to('@web')));


		$sec = Section::find()->where("parent='".$this->alias()."'")->with('sections')->all();


		//$context = Context::find()->with('projects')->all();

		//echo "<pre>";
		//print_r($context);
		//echo "</pre>";

		return $this->render($this->alias().'.twig', [	'sec' => $sec, ]);
    }



    public function actionPage()
    {
      if ($this->error) {
          throw new \yii\web\HttpException(404, 'The requested Item could not be found.');
      }

    // echo $this->alias(); exit;
    // echo EasyThumbnailImage::thumbnailFileUrl('@webroot/assets/img/sprites/spritesheet.png', 680, 300);

    $sec = Section::find()->where(["parent"=>$this->alias()])->with('sections')->orderBy('position')->all();
    // echo "<pre>";
    // foreach ($sec as $key => $value) {
    //   print_r($value->gallerys);
    // }
    // exit;

    $toView = ['sec' => $sec];

    if ($this->alias() == 'dev') {
      $expamples = Projectparam::find()
  			->where("active = '1'")
  			->with(['project', 'devsections'=>function($ds) {
  				$ds->orderBy('position');
  			}])
  			->orderby('cast(projectparam.release as unsigned) DESC, position')
  			->all();
      $toView['params'] = $expamples;
      $projectTypes = ProjectTypes::find()->all();
      $toView['types'] = $projectTypes;
    } else if ($this->alias() == 'context') {

      $context = Context::find()->with('projects')->all();
      $toView['context'] = $context;
    } else if ($this->alias() == 'about') {
      $expamples = Projectparam::find()
        ->where("active = '1' AND logo<>''")
        ->select("logo,id")
        ->orderby('cast(projectparam.release as unsigned) DESC, position')
        ->all();
      $toView['params'] = $expamples;
    }



    // echo "<pre>"; print_r($toView); exit;
    // echo $this->alias(); exit;

    return $this->render('url.twig', $toView);
    }

    public function actionSitemap()
    {
      $this->layout = false;
      Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
      $headers = Yii::$app->response->headers;
      $headers->add('Content-Type', 'text/xml; charset=utf-8');
      $srv = 'https://'.$_SERVER['SERVER_NAME']."/";
      $toView['server'] = $srv;

      $pages = Page::find()
        ->where("alias<>'index'")
        // ->orderby("parent DESC")
        ->all();

      $sections = [];
      $arr_pages = [];
      foreach ($pages as $key => $value) {
        $arr_pages[$value->id]['id'] = $value->id;
        $arr_pages[$value->id]['parent'] = $value->parent;
        $arr_pages[$value->id]['alias'] = $value->alias;
      }

      foreach ($arr_pages as $key => $value) {
        if ($value['parent'] == 0) {
          $sections[] = $srv.$value['alias']."/";
        } else {
          $sections[] = $srv.$arr_pages[$value['parent']]['alias']."/".$value['alias']."/";
        }
      }

      $toView['pages'] = $sections;

      $projects = Projectparam::find()
        ->where("active = '1' ")
        ->select("id")
        ->orderby('cast(projectparam.release as unsigned) DESC, position')
        ->all();
      $toView['projects'] = $projects;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://liderpoiska.ru/blog/ghost/api/v2/content/posts/?key=86e10fa5d6e38f168a7dfa7eab&limit=all");
		//curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('key' => '86e10fa5d6e38f168a7dfa7eab')));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		curl_close ($ch);
		
		if (!empty($res)) {
			$blogPosts = json_decode($res);
			if (!empty($blogPosts) && !empty($blogPosts->posts)) {
				foreach ($blogPosts->posts as $post) {
					$postUrls[] = str_replace('http://liderpoiska.ru', 'https://liderpoiska.ru', $post->url);
				}
				$toView['blogs'] = $postUrls;
			}
		}
		
      return $this->render('sitemap.twig', $toView);
    }

// 	public function actionDev()
//     {
//     $url = explode("/", Url::to('@web'));
// 		$alias = end($url);
//
//
// 		$sec = Section::find()->where("parent='".$this->alias()."'")->with('sections')->all();
//
// /*
// 		$projects = Project::find()
// 			->joinWith(['devsections'=>function($ds) {
// 				$ds->orderBy('devsection.position');
// 			}, 'projectparams'])
// 			->where(['and', "devsection.id > 0", "projectparam.active = '1'"])//, "projectparam.alias = 'dev'"
// 			->orderBy('projectparam.position')
// 			->all();
// */
// 		$expamples = Projectparam::find()
// 			->where("active = '1'")
// 			->with(['project', 'devsections'=>function($ds) {
// 				$ds->orderBy('position');
// 			}])
// 			->orderby('cast(projectparam.release as unsigned) DESC, position')
// 			->all();
// 		// echo "<pre>"; print_r($expamples); exit;
//
// 		return $this->render('url.twig', ['sec' => $sec, 'params' => $expamples]);
//     }
	public function actionDevshow()
    {
		$id=$this->alias();
/*
		$isset = Project::find()
			->joinWith(['devsections'=>function($ds) {
				$ds->orderBy('position');
			}])
			->where(['and', "devsection.id > 0", ['project.id'=>$id]])
			->one();
		if ( ! $isset) {
		   throw new HttpException(404, 'Страница не найдена'); exit;
		}
*/

		$expamples = Projectparam::find()
			->where("active = '1'")
			->with(['project', 'devsections'=>function($ds) {
				$ds->orderBy('position');
			}])
			->orderBy('position')
			->indexBy('id')
			->all();

		$thisExample = isset($expamples[$id]) ? $expamples[$id] : false ;
		if (!$thisExample) {
			throw new HttpException(404, 'Страница не найдена'); exit;
		}

		$page = [
      'seo_title' => 'Разработка сайта '.$thisExample->project->url,
      'seo_description' => 'Кейс по разработке сайта '.$thisExample->project->url.': этапы, сроки и стоимость разработки сайта от агентства интернет-маркетинга «Лидер Поиска»',
      'seo_keywords' => '',
      'alias' => 'dev_example',
      'name' => 'Сайт для '.$thisExample->project->url,
      'description' => $thisExample->project->description,
      'bg_head' => $thisExample->img_header,
      'projecturl' => $thisExample->project->url,
      'desk' => $thisExample->desk,
      'table' => $thisExample->table,
      'phone' => $thisExample->phone,
      'release' => $thisExample->release,
		];

    // echo "<pre>"; print_r($thisExample); echo "</pre>"; exit;
		$page = (object)$page;
		$this->page = $page;


		if (!empty($thisExample->desc_header)) $this->page->description = $thisExample->desc_header;
		if (!empty($thisExample->name)) $this->page->name = $thisExample->name;
		if (count($expamples)>1) {
			reset($expamples);
			while ($project = current($expamples)) {
				if (key($expamples) == $id) {
					prev($expamples);
					$prev= key($expamples);
					next($expamples);
					next($expamples);
					$next= key($expamples);
				}
				next($expamples);
			}

			if (isset($prev)) {
				$prevExample = $expamples[$prev];
			} else {
				end($expamples);
				$prev=key($expamples);
				$prevExample = $expamples[$prev];
			}
			if (isset($next)) {
				$nextExample = $expamples[$next];
			} else {
				reset($expamples);
				$next=key($expamples);
				if ($next == $id) {
					next($expamples);
					$next= key($expamples);
				}
				$nextExample = $expamples[$next];
			}
		} else {
			$prevExample = $expamples[$id];
			$nextExample = $expamples[$id];
		}

		$share = urlencode('url=http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'&amp;title='.$page->seo_title);


		return $this->render('dev_example.twig', [	'thisExample' 	=>	$thisExample,
													'nextExample'	=>	$nextExample,
													'prevExample'	=>	$prevExample,
													'share'			=>	$share,
												]);
    }

    public function actionLogin()
    {
		$this->layout = 'admin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {

		$sec = Section::find()->where("parent='".$this->alias()."'")->where(['or',['not', ['add_param_1'=>0]],['add_param_1'=>null]])->with('sections')->all();

		return $this->render($this->alias().'.twig', [
      'sec' => $sec
    ]);
    }

	public function actionProjectload($id=1)
    {
		$this->layout = false;
		if(!empty($_GET['id'])) {
			$page = $_GET['id'];
		} else {
			$page = 1;
		}
		
		$offset=$page*10;
		$nextOffset=$offset+10;

		$projects = Project::find()->where('active=1')->joinWith(['positions'=>function($q) {
			$q->where('weight>0')->orderBy('weight DESC');
		}])->with(['regions'=>function($query) {}])->andWhere(['>','position.weight', '0'])->groupBy('project.id')->offset($offset)->limit(20)->all();

		$cnt = count($projects);
		$num = (($cnt-10) > 0) ? $cnt-10 : 0;
		array_splice($projects,10);
        $a = $this->render('//sections/seo/portfolio/project_item_load.twig', [
            'projects' => $projects,
        ]);
		
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		echo json_encode(['html'=>$a, 'num' => $num]);
    }
	
	public function actionContextload($id=1)
    {
		$this->layout = false;
		if(!empty($_GET['id'])) $id=$_GET['id'];
		$model = Context::find()->where(['id'=>$id])->with('projects')->all();
		$model = $model[0];

        return $this->render('//sections/context/context_item.twig', [
            'c' => $model,
        ]);
    }
}
