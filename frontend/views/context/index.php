<?php

use yii\helpers\Html;
use yii\grid\GridView;


use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contexts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="context-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Context', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'search_request',
            'forecast_request_price',
            'result_request_price',
            'result_request_clicks',
            // 'result_compaign_price',
            // 'result_compaign_clicks',
            // 'forecast_img',
            // 'result_img',
			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/uploads/img/'.$data->forecast_img,
							100,
							50,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						return $e;
					}
				}
			],
			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/uploads/img/'.$data->result_img,
							100,
							50,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						return $e;
					}
				}
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
