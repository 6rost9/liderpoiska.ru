<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Context */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="context-form">

     <?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'search_request')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'forecast_request_price')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'result_request_price')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'result_request_clicks')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'result_compaign_price')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'result_compaign_clicks')->textInput(['maxlength' => 255]) ?>
	
	<?= $form->field($model, 'forecast_img')->fileInput() ?>
	
	<?= $form->field($model, 'result_img')->fileInput() ?>
	
	<?= $form->field($model, 'project_id')->dropDownList([
		ArrayHelper::map($project, 'id', 'name')
	]) ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
