<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Context */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Contexts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="context-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'search_request',
            'forecast_request_price',
            'result_request_price',
            'result_request_clicks',
            'result_compaign_price',
            'result_compaign_clicks',
            'forecast_img',
            'result_img',
        ],
    ]) ?>

</div>
