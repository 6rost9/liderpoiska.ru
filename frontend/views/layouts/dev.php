<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAssetLider;

use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this \yii\web\View */
/* @var $content string */

use yii\widgets\ListView;

//AppAssetLider::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?php echo Yii::$app->controller->page->seo_title; ?></title>
	<meta name="description" content="<?php echo Yii::$app->controller->page->seo_description; ?>">
	<meta name="keywords" content="<?php echo Yii::$app->controller->page->seo_keywords; ?>">
    <meta charset="<?= Yii::$app->charset ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= Html::encode($this->title) ?></title>

    <link rel="stylesheet" href="/assets/css/master.min.css">

    <!--[if lt IE 9]>
    <script src="js/fallback/html5shiv.js"></script>
    <script src="js/fallback/respond.min.js"></script>
    <script src="js/fallback/es5-shim.min.js"></script>
    <![endif]-->
	<?php $this->head() ?>
</head>

<body>

<?php $this->beginBody() ?>
<?php
	$headClass = false;
	$addMenu = false;
	$video=false;
	switch(Yii::$app->controller->page->alias) {
		case "about": $video=true; $headClass="b-header--about"; break;
		case "index": $video=true; $headClass="b-header--index"; break;
		case "seo":
		case "yandex":
		case "canpromote":
		case "portfolio": $addMenu = true; break;
	}
?>
<div class="b-page">
    <header class="b-header <?php if($headClass) echo $headClass; ?>"  <?php if (!$video):?> data-bg="/uploads/img/<?php echo (isset(Yii::$app->controller->page->bg_head) ? Yii::$app->controller->page->bg_head : ''); ?>" <?php endif; ?> >
		<?php if($video): ?>
        <div class="b-header__video">
            <video autoplay="" loop="">
                <source src="/uploads/video/<?php echo Yii::$app->controller->page->video; ?>" type="video/mp4;">
				<source src="/uploads/video/2.webm" type="video/webm;">
            </video>
        </div>
		<?php endif ?>


        <div class="b-header__top">

            <div class="b-header__container">
                <div class="b-header__logo-holder">

                    <a class="b-header__logo" href="/"></a>

                </div>

				<?php
				$menu = Yii::$app->controller->menu;
				echo '<nav class="b-header__menu">
				<ul class="menu">';
				foreach ($menu as $item) {
					if ($item->short_desc != '') $itemName = $item->short_desc;
					else $itemName = 'пункт меню';
					if (Yii::$app->controller->page->alias == $item->alias) $active='menu__item--active';
					else $active='';
					if ($item->id == 2) {
						if ($addMenu) {
							$active='menu__item--active';
							$c=1;
							$addMenu="<ul class='b-header__title_menu'>";
							foreach(Yii::$app->controller->addMenu as $addMenuItem) {
								if (Yii::$app->controller->page->alias == $addMenuItem->alias) $addMenuActive='menu__item--active menu__item--active-'.$c;
								else $addMenuActive='';
								$addMenu .= "
								<li class='menu__item $addMenuActive'>
									<a class='menu__link' href='/seo/". $addMenuItem->alias ."'>
										". $addMenuItem->short_desc ."
									</a>
								</li>
								";
								$c++;
							}
							$addMenu .= "</ul>";
						}
					}
					echo "
					<li class='menu__item $active'>
						<a class='menu__link' href='/". $item->alias ."'><span>". $itemName ."</span></a>
					</li>";
				}
				echo '</ul></nav>';
				?><!-- /.b-header__nav -->

                <div class="b-header__phone">
                    <p class="phone">
                        8 (800) 543-53-41
                    </p>
                    <p class="text">
                        <span>Бесплатно</span>
                        <span>по России</span>
                    </p>
                </div>
            </div>

        </div>
<div class="b-header__title-holder">
	<div class="b-header__title-wrapper">
		<div class="b-header__title">
			<?php if (Yii::$app->controller->page->alias=='index') echo "<p class='top'>Мы предлагаем</p>";?>
			<h1>
				<?php echo Yii::$app->controller->page->name; ?>
			</h1>
			<p class='bottom'>
				<?php if (Yii::$app->controller->page->alias=='index'):?>
					<a href="/seo">продвижение сайтов</a>,
					<a href="/context">контекстная реклама</a>,
					<a href="/dev">разработка сайтов</a>,
					<a href="/conversion">увеличение конверсии</a>
				<?php else: echo Yii::$app->controller->page->description; ?>

			</p>
			<?php endif ?>
		</div>
	</div>
	<?php if ($addMenu) echo $addMenu; ?>
</div>


</header><!-- /.b-header -->

		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= $content ?>
</div><!-- /.b-page -->

	<footer class="b-footer">
		<div class="b-footer__container">
			<a href="http://plusmedia.ru" class="b-footer__logo"></a>

			<div class="b-site-map">
				<div class="b-site-map__col">
					<h2 class="b-site-map__title"><a href="/seo">Продвижение сайта</a></h2>
					<ul class="b-site-map__list">
						<li class="list__item">— <a href="/seo/yandex">Новый алгоритм Яндекса</a></li>
						<li class="list__item">— <a href="/seo/canpromote">Можно ли продвинуть Ваш сайт?</a></li>
						<li class="list__item">— <a href="/seo/portfolio">Порфолио</a></li>
					</ul>
				</div>
				<div class="b-site-map__col">
					<h2 class="b-site-map__title">Другие услуги:</h2>
					<ul class="b-site-map__list">
						<li class="list__item">— <a href="/context">Контекстная реклама</a></li>
						<li class="list__item">— <a href="/conversion">Увеличение конверсии</a></li>
						<li class="list__item">— <a href="/dev">Разработка сайта</a></li>
					</ul>
				</div>
				<div class="b-site-map__col">
					<h2 class="b-site-map__title"><a href="/about">О компании</a></h2>
				</div>
			</div>
		</div>

	</footer><!-- /.b-footer -->
	<div class="popup"><p></p><button class="close">ОК</button></div>
		<div class="popup_bg"></div>
	<script src="/assets/js/build/production.min.js"></script>
	<script src="/assets/js/build/dev20.js"></script>
	<script src="/assets/js/inputmask.js"></script>
	<!-- <script src="/assets/js/mixitup.min.js"></script> -->
	<script src="/assets/js/isotope.pkgd.min.js"></script>
	<?php //$this->registerJsFile('@app/assets/js/script.js', self::POS_READY);?>
<?php //$this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
