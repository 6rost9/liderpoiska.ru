<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAssetLider;

use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this \yii\web\View */
/* @var $content string */

use yii\widgets\ListView;

//AppAssetLider::register($this);
?>
<?php $this->beginPage() ?>
<?php
	$page = Yii::$app->controller->page;
	if (!empty($page->seo_title)) $seo_title = $page->seo_title;
	else $seo_title='';
	if (!empty($page->seo_description))
		$seo_description = '<meta name="description" content="'.$page->seo_description .'">';
	else
		$seo_description='';
	if (!empty($page->seo_keywords))
		$seo_keywords = '<meta name="keywords" content="'.$page->seo_keywords .'">';
	else
		$seo_keywords='';


?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?php echo $seo_title; ?></title>
	<?php echo $seo_description; ?>
	<?php echo $seo_keywords; ?>
	<meta charset="<?= Yii::$app->charset ?>"/>
	<link rel="shortcut icon" href="/favicon.png" type="image/png">
	<meta name="viewport" content="width=device-width">
	<?php if (!empty($this->params['canonical'])) { ?>
		<link rel="canonical" href="<?php echo $this->params['canonical'] ?>">
	<?php } ?>
	<?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php
		if (strpos($_SERVER['REQUEST_URI'], '/dev/')!==false && $_SERVER['REQUEST_URI'] != '/dev/') $dev = true;
		else $dev = false;
	?>
	<?php
		if (strpos($_SERVER['REQUEST_URI'], '/about/')!==false && $_SERVER['REQUEST_URI'] != '/about/') $about = true;
		else $about = false;
	?>
	<?php if ($dev) : ?>
	<link rel="stylesheet" href="/assets/css/dev/swiper.min.css" />
	<link rel="stylesheet" href="/assets/css/dev/wdth-slider.css" />
	<link rel="stylesheet" href="/assets/js/dev/GammaGallery/css/style.css" />

	<link rel="stylesheet" href="/assets/css/dev/styles.css" />
	<?php endif ?>

	<?php if ($about) : ?>
	<link rel="stylesheet" href="/assets/css/dev/swiper.min.css" />
	<?php endif ?>

    <link rel="stylesheet" href="/assets/css/master.min.css">

    <!--[if lt IE 9]>
    <script src="js/fallback/html5shiv.js"></script>
    <script src="js/fallback/respond.min.js"></script>
    <script src="js/fallback/es5-shim.min.js"></script>
    <![endif]-->

	<?php $this->head() ?>
	<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '740418249420691'); // Insert your pixel ID here.
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=740418249420691&ev=PageView&noscript=1"
	/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
</head>

<body>

<?php $this->beginBody() ?>
<?php
	$headClass = false;
	$addMenu = false;
	$video=false;
	$titleHolder = true;
	if (!empty($page->alias)) {
		switch($page->alias) {
			case "about": $video=true; $headClass="b-header--about"; break;
			case "index": $video=true; $headClass="b-header--index"; break;
			case "dev_example": $headClass="b-header--case"; $titleHolder = true; break;
			case "seo":
			case "yandex":
			case "canpromote":
			case "portfolio": $addMenu = true; break;
		}
	}
	if (Html::encode($this->title)!='') {
		$headClass="b-header--error";
	}
?>
<div class="b-mobileMenu">
<?php
	$menu = Yii::$app->controller->menu;
	echo '<nav class="b-header__mobileMenu">
	<ul class="mobileMenu">';
	foreach ($menu as $item) {
		if ($item->short_desc != '') $itemName = $item->short_desc;
		else $itemName = 'пункт меню';
		$active='';
		if (!empty($page->alias) && $page->alias == $item->alias){
			$active='mobileMenu__item--active';
		} elseif (!empty($page->alias) && $page->alias == 'dev_example'){
			if ($item->alias == 'dev') $active='mobileMenu__item--active';
		}
		if ($item->id == 2) {
			if ($addMenu) {
				$active='mobileMenu__item--active';
				$c=1;
				$addMenu="<ul class='b-header__title_mobileMenu'>";
				foreach(Yii::$app->controller->addMenu as $addMenuItem) {
					if (Yii::$app->controller->page->alias == $addMenuItem->alias) $addMenuActive='mobileMenu__item--active mobileMenu__item--active-'.$c;
					else $addMenuActive='';
					$addMenu .= "
					<li class='mobileMenu__item $addMenuActive'>
						<a class='mobileMenu__link' href='/seo/". $addMenuItem->alias ."/'>
							". $addMenuItem->short_desc ."
						</a>
					</li>
					";
					$c++;
				}
				$addMenu .= "</ul>";
			}
		}
		echo "
		<li class='mobileMenu__item $active'>
			<a class='mobileMenu__link' href='/". $item->alias ."/'>". $itemName ."</a>
		</li>";
	}
	echo '</ul></nav>';
?><!-- /.b-mobileMenu -->
<div class="social_wrap">
	<a target="_blank" href="https://www.instagram.com/liderpoiska.ru/"><img src="/assets/img/inst_logo.svg" alt=""></a>
	<a target="_blank" href="https://www.facebook.com/liderpoiska.ru/"><img src="/assets/img/fb_logo.svg" alt=""></a>
</div>
</div>
<div class="b-burger_wrapper">
	<div class="b-burger">
		<div class="b-burger_item"></div>
		<div class="b-burger_item"></div>
		<div class="b-burger_item"></div>
	</div>
</div>
<div class="b-page">
    <header class="b-header <?php if($headClass) echo $headClass; ?>" <?php echo (isset(Yii::$app->controller->page->bg_head) ? "data-bg='/uploads/img/".Yii::$app->controller->page->bg_head."'" : ''); ?>>
		<?php if($video): ?>
		<?php $ext = explode('.', Yii::$app->controller->page->video); ?>
        <div class="b-header__video">
            <video autoplay="autoplay" loop="loop" poster="/assets/img/video.jpg">
                <source src="/uploads/video/<?php echo Yii::$app->controller->page->video; ?>" type="video/<?php echo $ext[1]; ?>;">
				<source src="/uploads/video/2.mp4" type="video/mp4;">
				<source src="/uploads/video/2.webm" type="video/webm;">
				<source src="/uploads/video/2.mkv" type="video/mkv;">
				<source src="/uploads/video/2.flv" type="video/flv;">
            </video>
        </div>
		<?php endif ?>


        <div class="b-header__top">

            <div class="b-header__container">
                <div class="b-header__logo-holder">

                    <a class="b-header__logo" href="/"></a>

                </div>

				<?php
				$menu = Yii::$app->controller->menu;
				echo '<nav class="b-header__menu">
				<ul class="menu">';
				// echo "<pre>";
				// print_r($menu);
				// echo "</pre>";
				// exit;
				foreach ($menu as $item) {
					if ($item->short_desc != '') $itemName = $item->short_desc;
					else $itemName = 'пункт меню';
					$active='';
					if (!empty($page->alias) && $page->alias == $item->alias){
						$active='menu__item--active';
					} elseif (!empty($page->alias) && $page->alias == 'dev_example'){
						if ($item->alias == 'dev') $active='menu__item--active';
					}
					if ($item->id == 2) {
						if ($addMenu) {
							$active='menu__item--active';
							$c=1;
							$addMenu="<ul class='b-header__title_menu'>";
							foreach(Yii::$app->controller->addMenu as $addMenuItem) {
								if (Yii::$app->controller->page->alias == $addMenuItem->alias) $addMenuActive='menu__item--active menu__item--active-'.$c;
								else $addMenuActive='';
								$addMenu .= "
								<li class='menu__item $addMenuActive'>
									<a class='menu__link' href='/seo/". $addMenuItem->alias ."/'>
										". $addMenuItem->short_desc ."
									</a>
								</li>
								";
								$c++;
							}
							$addMenu .= "</ul>";
						}
					}
					echo "
					<li class='menu__item $active'>
						<a class='menu__link' href='/". $item->alias ."/'>". $itemName ."</a>
					</li>";
				}
				echo '</ul></nav>';
				?><!-- /.b-header__nav -->
					<div class="b-header__phone <?php if (!empty($this->params['geo']['phone2'])) echo "two_phone"; ?> ">
						<?php if (empty($this->params['geo']['phone2'])) { ?>
							<p class="phone hide" data-geo-region="<?php	echo $this->params['geo']['alias'];	?>">
								<?php echo $this->params['geo']['phone']; ?>
							</p>
							<?php if (strpos($this->params['geo']['phone'], '8 (800)') !== false) { ?>
							<p class="text">
							<span>Бесплатно</span>
							<span>по России</span>
							</p>
							<?php } ?>
							<a class="email" href="mailto:info@liderpoiska.ru">info@liderpoiska.ru</a> <?
						} else { ?>
							<div class="phone_wrap">
								<p class="phone" data-geo-region="<?php	echo $this->params['geo']['alias2'];	?>">
									<?php echo $this->params['geo']['phone2']; ?>
								</p>
								<p class="text"><span>Бесплатно по России</span></p>
							</div>
							<a class="email f_mobile" href="mailto:info@liderpoiska.ru">info@liderpoiska.ru</a>
							<div class="phone_wrap hide"><p class="phone" data-geo-region="<?php	echo $this->params['geo']['alias'];	?>">
									<?php echo $this->params['geo']['phone']; ?>
								</p>
								<a class="email" href="mailto:info@liderpoiska.ru">info@liderpoiska.ru</a> </div>
							<?
						} ?>
						<div class="social_wrap">
							<a target="_blank" href="https://www.instagram.com/liderpoiska.ru/"><img src="/assets/img/inst_logo.svg" alt=""></a>
							<a target="_blank" href="https://www.facebook.com/liderpoiska.ru/"><img src="/assets/img/fb_logo.svg" alt=""></a>
						</div>

						</div>
          </div>
        </div>
<?php if($titleHolder): ?>
<div class="b-header__title-holder">
	<div class="b-header__title-wrapper">
		<div class="b-header__title">
			<?php if (!empty($page->alias) && $page->alias =='index') echo "<p class='top'>Мы предлагаем</p>";?>
			<h1>
				<?php if (!empty($page->name)) echo $page->name; ?>
			</h1>
			<?php if ($dev) echo "<a href='http://". $page->projecturl ."' target='_blank'>". frontend\components\Declension::cyrillic($page->projecturl) ."</a>";?>
			<p class='bottom'>
				<?php if (!empty($page->alias) && $page->alias=='index'):?>
					<a href="/seo/">продвижение сайтов</a>,
					<a href="/context/">контекстная реклама</a>,
					<a href="/dev/">разработка сайтов</a>,
					<a href="/conversion/">увеличение конверсии</a>
				<?php else: {
					if (!empty($page->description))
					echo $page->description;
				}
				?>

			</p>

			<? if ($dev){ ?>
				<div class="release">
					<span><?
					$rl = \frontend\components\Declension::month($page->release);
					echo \frontend\components\Declension::mb_ucfirst($rl);
					?></span>
				</div>
				<div class="adaptive_icons">
					<? if ($page->desk == 1){ ?>
					<div class="adaptive_icon">
						<img src="/assets/img/adaptive/desktop.png" alt="">
					</div>
					<?}?>
					<? if ($page->table == 1){ ?>
					<div class="adaptive_icon">
						<img src="/assets/img/adaptive/tablet.png" alt="">
					</div>
					<?}?>
					<? if ($page->phone == 1){ ?>
					<div class="adaptive_icon">
						<img src="/assets/img/adaptive/phone.png" alt="">
					</div>
					<?}?>
				</div>
				<?
				}
			?>
			<?php endif ?>
		</div>
	</div>
	<?php if ($addMenu) echo $addMenu; ?>
</div>
<?php endif ?>

</header><!-- /.b-header -->

		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= $content ?>
</div><!-- /.b-page -->

	<footer class="b-footer">
		<div class="b-footer__container">
			<div class="b-footer__logo">
				<a class="b-footer__logo-lp"></a>
				<a class="b-footer__logo-pm"></a>
			</div>
			<div class="b-site-map">
				<div class="b-site-map__col">
					<h2 class="b-site-map__title"><a href="/seo/">Продвижение сайта</a></h2>
					<ul class="b-site-map__list">
						<li class="list__item">— <a href="/seo/yandex/">Новый алгоритм Яндекса</a></li>
						<li class="list__item">— <a href="/seo/canpromote/">Можно ли продвинуть Ваш сайт?</a></li>
						<li class="list__item">— <a href="/seo/portfolio/">Портфолио</a></li>
					</ul>
				</div>
				<div class="b-site-map__col">
					<h2 class="b-site-map__title">Другие услуги:</h2>
					<ul class="b-site-map__list">
						<li class="list__item">— <a href="/context/">Контекстная реклама</a></li>
						<li class="list__item">— <a href="/conversion/">Увеличение конверсии</a></li>
						<li class="list__item">— <a href="/dev/">Разработка сайта</a></li>
					</ul>
				</div>
				<div class="b-site-map__col social">
					<h2 class="b-site-map__title"><a href="/about/">О компании</a></h2>
					<div class="social_wrap">
						<a target="_blank" href="https://www.instagram.com/liderpoiska.ru/"><img src="/assets/img/inst_logo.svg" alt=""></a>
						<a target="_blank" href="https://www.facebook.com/liderpoiska.ru/"><img src="/assets/img/fb_logo.svg" alt=""></a>
					</div>
				</div>
			</div>
		</div>

	</footer><!-- /.b-footer -->
	<div class="popup"><p></p><button class="close">ОК</button></div>
		<div class="popup_bg"></div>
	<script src="/assets/js/build/production.min.js"></script>
	<!-- <script src="/assets/js/mixitup.min.js"></script> -->
	<script src="/assets/js/isotope.pkgd.min.js"></script>
	<script src="/assets/js/build/dev20.js"></script>

	<script src="/assets/js/inputmask/inputmask.min.js"></script>
	<script src="/assets/js/inputmask/jquery.inputmask.min.js"></script>


	<?php if ($dev) : ?>
	<!-- before after -->
	<script src="/assets/js/dev/jquery.mousewheel-3.1.3.js" type="text/javascript"></script>
	<script src="/assets/js/dev/jquery.scrollpanel-0.5.0.js" type="text/javascript"></script>

	<!-- Swiper JS -->
	<script src="/assets/js/dev/swiper.jquery.js"></script>
	<!-- Initialize Swiper -->

	<script src="/assets/js/dev/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/assets/js/dev/jquery.beforeafter-1.4.min.js"></script>


	<script src="/assets/js/dev/GammaGallery/js/modernizr.custom.70736.js"></script>
	<script src="/assets/js/dev/GammaGallery/js/jquery.masonry.min.js"></script>
	<script src="/assets/js/dev/GammaGallery/js/gamma.js"></script>

	<script src="/assets/js/dev/scripts.js" type="text/javascript"></script>
	<?php endif ?>

	<?php if ($about) : ?>

	<!-- Swiper JS -->
	<script src="/assets/js/dev/swiper.jquery.js"></script>
	<!-- Initialize Swiper -->
	<?php endif ?>

<?php if(!YII_ENV || YII_ENV != 'dev') { ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142868776-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-142868776-1');
</script>
	
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter52110 = new Ya.Metrika({
                    id:52110,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52110" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '751980438609267'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" 
src="https://www.facebook.com/tr?id=751980438609267&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=186397aab09a18c6f48cef5793c5dfb4" charset="UTF-8" async></script>

<?php } ?>
<?php //$this->registerJsFile('@app/assets/js/script.js', self::POS_READY);?>
<?php //$this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
