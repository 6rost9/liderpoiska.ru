<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\components\ArrayMap;
/* @var $this yii\web\View */
/* @var $model app\models\Section */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>
	
	<?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

	<?= $form->field($model, 'parent')->dropDownList([
		ArrayMap::multilvl($parent, 'alias', 'name')
	]) ?>
	
    <?= $form->field($model, 'img')->textInput(['maxlength' => 255]) ?>
	
	<div class="form-group field-section-tpl has-success">
	<label class="control-label" for="section-tpl">Шаблон</label>
	<select id="section-tpl" class="form-control" name="Section[tpl]">
  	<?php
		
		foreach($tpls as $key => $val) {
			 
			if (is_array($val)) {
				echo "<optgroup label='$key'>";
				foreach($val as  $ke => $va) {
					if (is_array($va)) {
						echo "<optgroup label='$ke'>";
						foreach($va as $k => $v) {
							if (is_array($v)) {
								echo "<optgroup label='$k'>";
								foreach($v as $v4) {
									if (is_array($v4)) {
										echo "<option value='$v4'>------------</option>";
									} else echo "<option value='$v4'>$v4</option>";
								}
								echo "</optgroup>";
							} else echo "<option value='$v'>$v</option>";
						}
						echo "</optgroup>";
					} else
					echo "<option value='$va'>$va</option>";
				}
				
				echo "</optgroup>";
			} else {
				echo "<option value='$val'>$val</option>";
			}
		}
	?>
	</select>
	</div>
	
	<?= $form->field($model, 'add_param_1')->textarea(['rows' => 6]) ?>
	
    <?= $form->field($model, 'add_param_2')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
