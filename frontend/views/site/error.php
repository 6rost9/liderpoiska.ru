<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="b-case__content">

    <h3 class="error">
        Страница не найдена
    </h3>
	<p>Переход на <a href="/">главную</a></p>

</div>
