<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;;


/* @var $this yii\web\View */
/* @var $model app\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'reviewer')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'rank')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'project_id')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'review_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'img')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
