<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\imagine\Image;
use Imagine\Image\ImageInterface;

use himiklab\thumbnail\EasyThumbnailImage;

//$img = Image::thumbnail('@webroot/uploads/img/10020329(1).jpg', 120, 120)->show('jpg');
//exit; 
//use Imagine\Image\ImageInterface;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'reviewer',
            'rank',
            'project_id',
            'review_text:ntext',
            array(
				'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/uploads/img/'.$data->img,
							100,
							150,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						return $e;
					}
					
				}
			),
			

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
