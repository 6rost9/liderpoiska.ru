<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Param */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="param-form">

    <?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
	
	<?= $form->field($model, 'group_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'value')->textInput() ?>

	<?= $form->field($model, 'img')->fileInput() ?>
	
	<?= $form->field($model, 'project_id')->dropDownList([
		ArrayHelper::map($project, 'id', 'name')
	]) ?>
	
	<?= $form->field($model, 'default')->textInput(['maxlength' => 255]) ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
