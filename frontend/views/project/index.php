﻿<?php

use yii\helpers\Html;
use yii\grid\GridView;


use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'url:url',
            'description',
			'short_desc',
            'region',
            // 'startdate',
            // 'active',
			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/uploads/img/logo'.$data->img,
							100,
							50,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						return $e;
					}
				}
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
