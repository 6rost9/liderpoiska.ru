<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

     <?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textInput() ?>
	
	<?= $form->field($model, 'short_desc')->textInput(['maxlength' => 255]) ?>

    <?// $form->field($model, 'region')->textInput(['maxlength' => 255]) ?>
	
	<?= $form->field($model, 'region')->dropDownList([
		ArrayHelper::map($region, 'id', 'name')
	]) ?>
	
    <?= $form->field($model, 'startdate')->textInput() ?>

    <?= $form->field($model, 'active')->dropDownList([
        '1'=>'Активен',
        '0'=>'Неактивен'
    ]) ?>

	<?= $form->field($model, 'img')->fileInput() ?>
	
	<?= $form->field($model, 'show_in_slider')->checkBox(['label' => 'Показывать в слайдере', 'uncheck' => null, 'selected' => true]) ?>
	
	<?= $form->field($model, 'dev')->checkBox(['label' => 'Разрабатывали сайт?', 'uncheck' => null, 'selected' => true]) ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
