<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    // 'vendorPath' => dirname(DIR) . '/vendor',
    'id' => 'liderpoiska',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
      'thumbnail' => [
        'class' => 'himiklab\thumbnail\EasyThumbnail',
        'cacheAlias' => 'assets/gallery_thumbnails',
    ],
      'view' => [
          'class' => 'yii\web\View',
          'renderers' => [
      //'layout'=>false,
              'tpl' => [
                  'class' => 'yii\smarty\ViewRenderer',
                  //'cachePath' => '@runtime/Smarty/cache',
              ],
              'twig' => [
                  'class' => 'yii\twig\ViewRenderer',
                  'cachePath' => '@runtime/Twig/cache',
                  // Array of twig options:
                  'options' => ['auto_reload' => true],
                  'globals' => [
                    'html' => '\yii\helpers\Html',
                    'name' => 'Carsten',
                    'GridView' => '\yii\grid\GridView',
                    'ListView' => '\yii\widgets\ListView',
                    'EasyThumbnailImage' => '\himiklab\thumbnail\EasyThumbnailImage',
                    // 'Url' => '\yii\helpers\Url',
                    'decl' => 'frontend\components\Declension',
                    //'form' => 'yii\widgets\ActiveForm',
                  ],
              ],
              // ...
          ],
      ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
    			//'suffix' => '/',
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                ['pattern'=>'order/<action>','route'=>'order/<action>', 'suffix'=>'/'],
                // ['pattern'=>'<action:(seo|about|context|yandex|canpromote|portfolio|conversion|dev|contextload|projectload)>','route'=>'site/<action>', 'suffix'=>'/'],
                ['pattern'=>'<action:(contextload|projectload|seo)>','route'=>'site/<action>', 'suffix'=>'/'],
            		['pattern'=>'<action>','route'=>'site/page', 'suffix'=>'/'],
            		['pattern'=>'seo/<action:(portfolio|yandex|canpromote)>','route'=>'site/<action>', 'suffix'=>'/'],
            		['pattern'=>'dev/<id:\w+>','route'=>'site/devshow','suffix'=>'/'],
            		['pattern'=>'position/<action>','route'=>'position/<action>','suffix'=>'/'],
                ['pattern'=>'<action>/<action2>','route'=>'site/page', 'suffix'=>'/'],
                ['pattern' => 'sitemap', 'route' => 'site/sitemap', 'suffix' => '.xml', ]
            ],
        ]
    ],
    'params' => $params,
];
