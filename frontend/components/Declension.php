<?php

namespace frontend\components;

use Yii;

class Declension {
    public static function click($num) {
		$ost=$num%10;
		if ($ost!=0) {
			switch ($ost) {
				case 1:	$word='переход';	break;
				case 2:
				case 3:
				case 4: $word='перехода'; break;
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:	$word='переходов'; break;
			}
		} else $word='переходов';
		return $num." ".$word;
	}
	public static function rub($num) {
		$ost=$num%10;
		if ($ost!=0) {
			switch ($ost) {
				case 1:	$word='рубль';	break;
				case 2:
				case 3:
				case 4: $word='рубля'; break;
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:	$word='рублей'; break;
			}
		} else $word='рублей';
		return $num." ".$word;
	}
  public static function mb_ucfirst($text) {
    mb_internal_encoding("UTF-8");
      return mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
  }
	public static function project($num) {
		$ost=$num%10;
		if ($ost!=0) {
			switch ($ost) {
				case 1:	$word='проект';	break;
				case 2:
				case 3:
				case 4: $word='проекта'; break;
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:	$word='проектов'; break;
			}
		} else $word='проектов';
		return $num." ".$word;
	}
	public static function visit($num) {
		if ($num%10==1 && $num%100!=11) $word='перехода';
		else $word='переходов';
		return $word;
	}
	public static function cyrillic($url) {
		return idn_to_utf8($url);
	}
	public static function month($date) {
		$m = date("m", $date);
		$y = date("Y", $date);
		$months = [
			'01'=>'январь',
			'02'=>'февраль',
			'03'=>'март',
			'04'=>'апрель',
			'05'=>'май',
			'06'=>'июнь',
			'07'=>'июль',
			'08'=>'август',
			'09'=>'сентябрь',
			'10'=>'октябрь',
			'11'=>'ноябрь',
			'12'=>'декабрь'
		];
		return (isset($months[$m]) ? $months[$m] : '').' '.$y;
	}
}
