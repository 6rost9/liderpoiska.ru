<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $short_desc
 * @property integer $position
 * @property string $parent
 * @property string $img
 * @property string $add_param_1
 * @property string $add_param_2
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'short_desc', 'add_param_1', 'add_param_2', 'content'], 'string'],
            [['position'], 'integer'],
            [['name', 'parent', 'img', 'tpl'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'short_desc' => 'Короткое описание',
			'content' => 'Контент',
            'position' => 'Позиция',
            'parent' => 'Родитель',
            'img' => 'Картинка',
			'add_param_1' => 'Шаблон',
            'add_param_1' => 'Доп параметр 1',
            'add_param_2' => 'Доп параметр 2',
        ];
    }
	public function getSections()
    {
        return $this->hasMany(Section::className(), ['parent' => 'id']);
    }
    public function getGallerys() {
		return $this->hasMany(Gallery::className(), ['elem_id'=>'id'])->where(['model'=>Section::tableName()]);;
	}
}
