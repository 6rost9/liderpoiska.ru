<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "param".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $value

 */
class Projectparam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projectparam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'value', 'project_id'], 'required'],
            [['name', 'alias', 'value', 'project_id', 'group_name'], 'string', 'max' => 255],
            [['img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
			'group_name' => 'Группа параметров',
            'alias' => 'Синоним(alias)',
            'value' => 'Значение',
            'project_id' => 'Проект',
			'img' => 'Картинка',
        ];
    }

	public function getProject() {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
	public function getDevsections() {
        return $this->hasMany(Devsection::className(), ['projectparam_id' => 'id']);
    }
}
