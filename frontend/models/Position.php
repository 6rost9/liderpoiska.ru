<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "position".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $ap_id
 * @property string $query
 * @property integer $wordstat
 * @property integer $position
 * @property integer $prev_position
 */
class Position extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'ap_id', 'query', 'wordstat', 'position', 'prev_position'], 'required'],
            [['project_id', 'ap_id', 'wordstat', 'position', 'prev_position'], 'integer'],
            [['query'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'ap_id' => 'Ap ID',
            'query' => 'Query',
            'wordstat' => 'Wordstat',
            'position' => 'Position',
            'prev_position' => 'Prev Position',
        ];
    }

	public function getProjects()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
