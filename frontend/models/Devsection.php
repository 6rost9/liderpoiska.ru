<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "devsection".
 *
 * @property integer $id
 * @property string $name
 * @property integer $position
 * @property string $description
 * @property string $content
 * @property integer $parent
 * @property string $img
 * @property string $tpl
 * @property string $add_param_1
 * @property string $add_param_2
 */
class Devsection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'devsection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['position', 'project_id',  'gray_bg'], 'integer'],
            [['description', 'content', 'after_text', 'task'], 'string'],
            [['name', 'tpl', 'label'], 'string', 'max' => 255],
			[['img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position' => 'Position',
            'description' => 'Description',
            'content' => 'Content',
            'project_id' => 'Проект',
            'img' => 'Превью в шапке',
            'tpl' => 'Шаблон',
            'gray_bg' => 'Серый фон',
            'label' => 'Ярлычок',
			'after_text' => 'Текст после блоков',
			'task' => 'Задача',
        ];
    }
	public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
	public function getGallerys() {
		return $this->hasMany(Gallery::className(), ['elem_id'=>'id'])->where(['model'=>Devsection::tableName()]);;
	}
}
