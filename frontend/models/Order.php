<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $url
 * @property string $description
 * @property string $manager
 * @property integer $date
 * @property integer $processed
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'processed', 'seo', 'context', 'conversion'], 'integer'],
            [['name', 'email', 'phone', 'url', 'description', 'manager'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'seo' => 'SEO',
			'conversion' => 'Конверсия',
			'context' => 'Контекст',
            'name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'url' => 'Url',
            'description' => 'Description',
            'manager' => 'Manager',
            'date' => 'Date',
            'processed' => 'Processed',
        ];
    }
}
