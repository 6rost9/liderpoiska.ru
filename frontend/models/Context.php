<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "context".
 *
 * @property integer $id
 * @property string $search_request
 * @property string $forecast_request_price
 * @property string $result_request_price
 * @property string $result_request_clicks
 * @property string $result_compaign_price
 * @property string $result_compaign_clicks
 * @property string $result_img
 */
class Context extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'context';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search_request', 'forecast_request_price', 'result_request_price', 'result_request_clicks', 'result_compaign_price', 'result_compaign_clicks', 'project_id'], 'required'],
            [['search_request', 'forecast_request_price', 'result_request_price', 'result_request_clicks', 'result_compaign_price', 'result_compaign_clicks', 'project_id'], 'string', 'max' => 255],
			[['forecast_img', 'result_img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'search_request' => 'Запрос',
            'forecast_request_price' => 'Цена запроса - проноз',
            'result_request_price' => 'Цена запроса - итог',
            'result_request_clicks' => 'Переходов по запросу - итог',
            'result_compaign_price' => 'Цена за компанию - итог',
            'result_compaign_clicks' => 'Переходов за компанию - итог',
            'forecast_img' => 'Картинка прогноза',
            'result_img' => 'Картинка результата',
        ];
    }
	public function getProjects() {
		return $this->hasOne(Project::className(), ['id'=>'project_id']);
	}

}
