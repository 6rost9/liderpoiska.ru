<?php

namespace console\controllers;

use Yii;
use backend\models\Position;
use backend\models\Project;
#use yii\data\ActiveDataProvider;
use yii\console\Controller;
use yii\filters\VerbFilter;
use console\components\AllPositions;


/**
 * PositionController implements the CRUD actions for Position model.
 */
class PositionController extends Controller
{
	public $layout = 'admin';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	public function actionGetpositions()
    {

		$api = new AllPositions('2f1cc568373cedcc63d17a1938cf6221');
		echo "<pre>";
		$start = microtime(true);
		$projects = Project::find()->where("active='1'")->where("date<'".date('Y-m-d H-i-s', time()-60*60*24)."'")->limit(20)->all();
		//$i=0;
		$k=[1=>1,
			2=>1,
			3=>1,
			4=>.85,
			5=>.6,
			6=>.5,
			7=>.5,
			8=>.3,
			9=>.3,
			10=>.2];
		foreach ($projects as $project) {
			//echo $project->date;continue;
			//if ($i<=5) {//$project['id_project'] <= 130609
				$report = $api->get_report($project->ap_id);

				$project->top3 = $report['top3'];
				$project->top10 = $report['top10'];
				$project->top30 = $report['top30'];
				$project->date = date("Y-m-d H:i:s");
				$project->save();
				//print_R($project);
				if ($project->save()) {
					//echo "обновил " . $project->url. "  <br>";
				} else {
					//echo "НЕ обновил " . $project->url. "  !!!!!!!<br>";
				}
				$sengines = is_array($report['sengines']) ? $report['sengines'] : array($report['sengines']);
				$id_se = false;
				foreach ($sengines as $seng) {
					if ($seng['name_se'] == 'Яндекс') {
						$id_se = $seng['id_se'];
					}
				}
				$queries = is_array($report['queries']) ? $report['queries'] : array($report['queries']);

				if ($id_se) {
				foreach ($queries as $query) {
					$pos_id = $id_se."_".$query['id_query'];
					if (($model = Position::find()->where("ap_id='$query[id_query]'")->one()) !== null) {
						$model->wordstat = $query['wordstat'];
						$model->position = $report['positions']["$pos_id"]['position'];
						$model->prev_position = $report['positions']["$pos_id"]['prev_position'];
						if ($model->position < 11 && $model->position > 0) {
							$model->weight = $k[$model->position] * $model->wordstat;
						} else $model->weight = 0;
						$model->save();
					} else {
						$model = new Position();
						$model->project_id = $project->id;
						$model->ap_id = $query['id_query'];
						$model->query = $query['query'];
						$model->wordstat = $query['wordstat'];
						//$pos = $report['positions']["$pos_id"]['position'];
						if (!$report['positions']["$pos_id"]['position']) continue;
						$model->position = $report['positions']["$pos_id"]['position'];
						$model->prev_position = $report['positions']["$pos_id"]['prev_position'];
						if ($model->position < 11 && $model->position > 0) {
							$model->weight = $k[$model->position] * $model->wordstat;
						} else $model->weight = 0;
						$model->save();
					}
				}
			}
		//	} $i++;
		}
		/*print_r(
			$api->get_projects()
			//$api->get_report(184855)
		);*/
		//print_r($api->get_projects_group());

		echo microtime(true) - $start .'<br>';
		echo "Обновление прошло успешно";
    }
	public function actionAp()
    {

		$api = new AllPositions('2f1cc568373cedcc63d17a1938cf6221');
		//echo "<pre>";
				$archiveProjects = $api->get_projects(11926);
		foreach ($archiveProjects as $p) {
			if (($model = Project::find()->where("url='$p[url]'")->one()) !== null) {
				if ($model->active != 0) {
					$model->active = 0;
					$model->save();
					//echo "Обновил $p[url]! Неактивен!<br>";
				}
			} else {
				//echo "Не найден $p[url]<br>";
			}
		}
		$activeProjects = $api->get_projects(10463);
		foreach ($activeProjects as $p) {
			if (($model = Project::find()->where("url='$p[url]'")->one()) !== null) {
				if ($model->active != 1) {
					$model->active = 1;
					$model->save();
					//echo "Обновил $p[url]!<br>";
				}
			} else {
				$model = new Project();
				$model->ap_id = $p['id_project'];
				$model->url = $p['url'];
				$model->cy = $p['cy'];
				$model->pr = $p['pr'];
				$model->name = $p['url'];
				$model->active = 1;
				$model->description = '-';
				$model->short_desc = '-';
				$model->region = 0;
				$model->startdate = date("Y");
				$model->img = "-";
				$model->show_in_slider = 0;
				$model->dev = 0;
				$model->top3 = 0;
				$model->top10 = 0;
				$model->top30 = 0;
				$model->date = date("Y-m-d H:i:s");
				$model->save();
				//echo "Добавил $p[url]!<br>";
			}
		}

/*
		$passiveProjects = $api->get_projects(11745);
		foreach ($activeProjects as $p) {
			if (($model = Project::find()->where("url='$p[url]'")->one()) !== null) {
				if ($model->active == 1) {
					$model->active = 0;
					$model->save();
					echo "Деактивировал $p[url]!<br>";
				}
			}
		}
*/
/*
		print_r(
			$api->get_projects(10463)
			//$api->get_report(184855)
		);
*/
		//print_r($api->get_projects_group());

		echo 'Обновление прошло успешно';
    }
}
