<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_150609_projectparam_edit_global extends Migration
{
    public function up()
    {
		$this->renameColumn( 'projectparam', 'alias', 'after_dev' );
		$this->renameColumn( 'projectparam', 'value', 'task' );
		$this->renameColumn( 'projectparam', 'default', 'desc_header' );
		$this->renameColumn( 'projectparam', 'group_name', 'img_header' );
		
		$this->alterColumn( 'projectparam', 'after_dev', Schema::TYPE_TEXT);
		$this->addColumn('projectparam', 'release', Schema::TYPE_STRING);
		$this->addColumn('projectparam', 'active', Schema::TYPE_INTEGER);
    }

    public function down()
    {
		$this->renameColumn( 'projectparam', 'after_dev', 'alias' );
		$this->renameColumn( 'projectparam', 'task', 'value' );
		$this->renameColumn( 'projectparam', 'desc_header', 'default' );
		$this->renameColumn( 'projectparam', 'img_header', 'group_name' );
		
		$this->alterColumn( 'projectparam', 'alias', Schema::TYPE_STRING);
		$this->dropColumn('projectparam', 'release');
		$this->dropColumn('projectparam', 'active');
    }
}
