<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_152714_projectparam_value_upd extends Migration
{
    public function up()
    {
		$this->alterColumn( 'projectparam', 'value', Schema::TYPE_TEXT);
    }

    public function down()
    {
		$this->alterColumn( 'projectparam', 'value', Schema::TYPE_STRING);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
