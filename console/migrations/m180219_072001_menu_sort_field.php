<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180219_072001_menu_sort_field
 */
class m180219_072001_menu_sort_field extends Migration
{
  public function up()
  {
    $this->addColumn('page', 'sort', Schema::TYPE_INTEGER);
  }

  public function down()
  {
    $this->dropColumn('page', 'sort', Schema::TYPE_INTEGER);
  }
}
