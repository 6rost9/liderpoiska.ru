<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171227_104737_new_project_params
 */
class m171227_104737_new_project_params extends Migration
{
  public function up()
  {
    $this->addColumn('projectparam', 'logo', Schema::TYPE_STRING);
    $this->addColumn('projectparam', 'bg_image', Schema::TYPE_STRING);
    $this->addColumn('projectparam', 'teaser', Schema::TYPE_STRING . ' NOT NULL');
    $this->addColumn('projectparam', 'bg_color', Schema::TYPE_STRING);
    $this->addColumn('projectparam', 'teaser_white', Schema::TYPE_INTEGER);
    $this->addColumn('projectparam', 'teaser_sub', Schema::TYPE_INTEGER);
    $this->addColumn('projectparam', 'project_type_id', Schema::TYPE_INTEGER);
  }

  public function down()
  {
    $this->dropColumn('projectparam', 'logo', Schema::TYPE_STRING);
    $this->dropColumn('projectparam', 'bg_image', Schema::TYPE_STRING);
    $this->dropColumn('projectparam', 'teaser', Schema::TYPE_STRING . ' NOT NULL');
    $this->dropColumn('projectparam', 'bg_color', Schema::TYPE_STRING);
    $this->dropColumn('projectparam', 'teaser_white', Schema::TYPE_INTEGER);
    $this->dropColumn('projectparam', 'teaser_sub', Schema::TYPE_INTEGER);
    $this->dropColumn('projectparam', 'project_type_id', Schema::TYPE_INTEGER);
  }
}
