<?php

use yii\db\Schema;
use yii\db\Migration;

class m150518_115358_update_regions extends Migration
{
    public function up()
    {
        $this->addColumn('region', 'region_full',Schema::TYPE_STRING);
		$this->addColumn('region', 'sorting', Schema::TYPE_INTEGER);
        //$this->createIndex('url', '{{%context}}', 'url', true);

    }

    public function down()
    {
        $this->dropColumn('region','region_full');
		$this->dropColumn('region','sorting');
    }
}
