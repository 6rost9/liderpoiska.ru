<?php

use yii\db\Schema;
use yii\db\Migration;

class m141205_073508_context extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%context}}', [
            'id' => Schema::TYPE_PK,
			'project_id'=> Schema::TYPE_INTEGER . ' NOT NULL',
            'search_request' => Schema::TYPE_STRING . ' NOT NULL',
            'forecast_request_price' => Schema::TYPE_STRING . ' NOT NULL',
			'result_request_price' => Schema::TYPE_STRING . ' NOT NULL',
			'result_request_clicks' => Schema::TYPE_STRING . ' NOT NULL',
			'result_compaign_price' => Schema::TYPE_STRING . ' NOT NULL',
			'result_compaign_clicks' => Schema::TYPE_STRING . ' NOT NULL',
			'forecast_img' => Schema::TYPE_STRING,
			'result_img' => Schema::TYPE_STRING,
        ], $tableOptions);
        //$this->createIndex('url', '{{%context}}', 'url', true);

    }

    public function down()
    {
        $this->dropTable('{{%context}}');
    }
}