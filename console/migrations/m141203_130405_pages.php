<?php

use yii\db\Schema;
use yii\db\Migration;

class m141203_130405_pages extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%page}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
			'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT,
			'short_desc' => Schema::TYPE_TEXT,
			'content' => Schema::TYPE_TEXT,
            'seo_title' => Schema::TYPE_STRING,
			'seo_description' => Schema::TYPE_TEXT,
			'seo_keywords' => Schema::TYPE_STRING,
            'parent' => Schema::TYPE_INTEGER,
            'active' => Schema::TYPE_INTEGER,
			'url' => Schema::TYPE_STRING,
			'img' => Schema::TYPE_STRING,
			'video' => Schema::TYPE_STRING,
			'bg_head' => Schema::TYPE_STRING,
        ], $tableOptions);
        $this->createIndex('alias', '{{%page}}', 'alias', true);

    }

    public function down()
    {
        $this->dropTable('{{%page}}');
    }
}
