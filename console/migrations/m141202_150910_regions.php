<?php

use yii\db\Schema;
use yii\db\Migration;

class m141202_150910_regions extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

		$this->createTable('{{%region}}', [
            'id' => Schema::TYPE_PK,
			'name'=> Schema::TYPE_STRING . ' NOT NULL',
			'show_in_slider'=> Schema::TYPE_INTEGER,
        ], $tableOptions);
        //$this->createIndex('alias', '{{%region}}', 'alias', true);
    }

    public function down()
    {
        $this->dropTable('{{%region}}');
    }
}
