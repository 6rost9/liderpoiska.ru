<?php

use yii\db\Schema;
use yii\db\Migration;

class m160121_080929_devsec_upd_project_too_pp extends Migration
{
    public function up()
    {
		$this->renameColumn( 'devsection', 'project_id', 'projectparam_id' );
    }

    public function down()
    {
        $this->renameColumn( 'devsection', 'projectparam_id', 'project_id' );
    }

}
