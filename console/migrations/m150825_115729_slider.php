<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_115729_slider extends Migration
{
    public function up()
    {
		$this->createTable('{{%slider}}', [
            'id' => Schema::TYPE_PK,
            'project_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'period_1' => Schema::TYPE_STRING,
            'val_1' => Schema::TYPE_INTEGER,
            'period_2' => Schema::TYPE_STRING,
            'val_2' => Schema::TYPE_INTEGER,
			'period_3' => Schema::TYPE_STRING,
            'val_3' => Schema::TYPE_INTEGER,
			'period_4' => Schema::TYPE_STRING,
            'val_4' => Schema::TYPE_INTEGER,
			'period_5' => Schema::TYPE_STRING,
            'val_5' => Schema::TYPE_INTEGER,
			'per_day' => Schema::TYPE_INTEGER,
			'per_month' => Schema::TYPE_INTEGER,
            'img' => Schema::TYPE_STRING,
			'param' => Schema::TYPE_STRING
        ]);
    }

    public function down()
    {
		$this->dropTable('{{%slider}}');
    }
}
