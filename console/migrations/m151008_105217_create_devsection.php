<?php

use yii\db\Schema;
use yii\db\Migration;

class m151008_105217_create_devsection extends Migration
{
    public function up()
    {
		$this->createTable('{{%devsection}}', [
            'id' => Schema::TYPE_PK,
			'name'=> Schema::TYPE_STRING . ' NOT NULL',
			'position' => Schema::TYPE_INTEGER,
            'description' => Schema::TYPE_TEXT,
			'content' => Schema::TYPE_TEXT,
			'project_id' => Schema::TYPE_INTEGER,
			'img' => Schema::TYPE_STRING,
			'tpl' => Schema::TYPE_STRING,
			'gray_bg'=> Schema::TYPE_INTEGER,
			'label'=> Schema::TYPE_STRING,
			'after_text' => Schema::TYPE_TEXT,
			'task' => Schema::TYPE_STRING,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%devsection}}');
    }
}
