<?php

use yii\db\Schema;
use yii\db\Migration;

class m141117_134159_reviews extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%review}}', [
            'id' => Schema::TYPE_PK,
            'reviewer' => Schema::TYPE_STRING . ' NOT NULL',
            'rank' => Schema::TYPE_STRING . ' NOT NULL',
            'project_id' => Schema::TYPE_STRING . ' NOT NULL',
            'review_text' => Schema::TYPE_TEXT,
            'img' => Schema::TYPE_STRING,
        ], $tableOptions);
        //$this->createIndex('url', '{{%review}}', 'url', true);
    }

    public function down()
    {
		$this->dropTable('{{%review}}');
    }
}