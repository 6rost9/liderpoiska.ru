<?php

use yii\db\Schema;
use yii\db\Migration;

class m150203_120651_order extends Migration
{
    public function up()
    {
		 $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%order}}', [
            'id' => Schema::TYPE_PK,
			'name'=> Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'phone' => Schema::TYPE_STRING,
			'url' => Schema::TYPE_STRING,
			'description' => Schema::TYPE_STRING,
			'seo' => Schema::TYPE_INTEGER,
			'conversion' => Schema::TYPE_INTEGER,
			'context' => Schema::TYPE_INTEGER,
			'manager' => Schema::TYPE_STRING,
			'date' => Schema::TYPE_INTEGER,
			'processed' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        //$this->createIndex('url', '{{%order}}', 'url', true);

    }

    public function down()
    {
        $this->dropTable('{{%order}}');
    }
}
