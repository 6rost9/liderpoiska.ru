<?php

use yii\db\Schema;
use yii\db\Migration;

class m141111_081434_section extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

		$this->createTable('{{%section}}', [
            'id' => Schema::TYPE_PK,
			'name'=> Schema::TYPE_STRING . ' NOT NULL',
//			'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT,
			'short_desc' => Schema::TYPE_TEXT,
			'content' => Schema::TYPE_TEXT,
			'position' => Schema::TYPE_INTEGER,
			'parent' => Schema::TYPE_STRING,
			'img' => Schema::TYPE_STRING,
			'tpl' => Schema::TYPE_STRING,
			'add_param_1'=> Schema::TYPE_TEXT,
			'add_param_2'=> Schema::TYPE_TEXT,
        ], $tableOptions);
        //$this->createIndex('alias', '{{%section}}', 'alias', true);

    }

    public function down()
    {
        $this->dropTable('{{%section}}');
    }
}
