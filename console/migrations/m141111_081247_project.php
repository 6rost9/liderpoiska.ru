<?php

use yii\db\Schema;
use yii\db\Migration;

class m141111_081247_project extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%project}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'url' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT,
			'short_desc' => Schema::TYPE_TEXT,
            'region' => Schema::TYPE_INTEGER . ' NOT NULL',
            'startdate' => Schema::TYPE_INTEGER,
            'active' => Schema::TYPE_INTEGER,
			'img' => Schema::TYPE_STRING,
			'show_in_slider' => Schema::TYPE_INTEGER,
			'dev' => Schema::TYPE_INTEGER,
			'ap_id' => Schema::TYPE_INTEGER,
			'cy' => Schema::TYPE_INTEGER,
			'pr' => Schema::TYPE_INTEGER,
			'top3' => Schema::TYPE_INTEGER,
			'top10' => Schema::TYPE_INTEGER,
			'top30' => Schema::TYPE_INTEGER,
			'date' => Schema::TYPE_TIMESTAMP,
        ], $tableOptions);
        $this->createIndex('url', '{{%project}}', 'url', true);

    }

    public function down()
    {
        $this->dropTable('{{%project}}');
    }
}
