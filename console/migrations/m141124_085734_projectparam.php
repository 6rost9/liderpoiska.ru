<?php

use yii\db\Schema;
use yii\db\Migration;

class m141124_085734_projectparam extends Migration
{
   public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

		$this->createTable('{{%projectparam}}', [
            'id' => Schema::TYPE_PK,
			'group_name'=> Schema::TYPE_STRING,
			'name'=> Schema::TYPE_STRING . ' NOT NULL',
			'alias' => Schema::TYPE_STRING . ' NOT NULL',
            'value' => Schema::TYPE_STRING . ' NOT NULL',
            'project_id' => Schema::TYPE_STRING . ' NOT NULL',
			'img' => Schema::TYPE_STRING,
			'default'=> Schema::TYPE_TEXT
        ], $tableOptions);
        //$this->createIndex('alias', '{{%projectparam}}', 'alias', true);

    }

    public function down()
    {
        $this->dropTable('{{%projectparam}}');
    }
}
