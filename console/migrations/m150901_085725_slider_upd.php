<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_085725_slider_upd extends Migration
{
    public function up()
    {
		$this->alterColumn( 'slider', 'per_day', Schema::TYPE_STRING);
		$this->alterColumn( 'slider', 'per_month', Schema::TYPE_STRING);
		$this->renameColumn( 'slider', 'per_day', 'note_top');
		$this->renameColumn( 'slider', 'per_month', 'note_bottom');
    }

    public function down()
    {
        $this->renameColumn( 'slider', 'note_top', 'per_day');
		$this->renameColumn( 'slider', 'note_bottom', 'per_month');
		$this->alterColumn( 'slider', 'per_day', Schema::TYPE_INTEGER);
		$this->alterColumn( 'slider', 'per_month', Schema::TYPE_INTEGER);
    }
}
