<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m171227_111854_project_types
 */
class m171227_111854_project_types extends Migration
{
  public function up()
  {
  $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
      }

  $this->createTable('{{%project_types}}', [
          'id' => Schema::TYPE_PK,
          'name'=> Schema::TYPE_STRING . ' NOT NULL',
      ], $tableOptions);
  }

  public function down()
  {
      $this->dropTable('{{%project_types}}');
  }
}
