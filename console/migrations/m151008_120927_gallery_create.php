<?php

use yii\db\Schema;
use yii\db\Migration;

class m151008_120927_gallery_create extends Migration
{
    public function up()
    {
		$this->createTable('{{%gallery}}', [
			'id' => Schema::TYPE_PK,
			'path'=> Schema::TYPE_STRING . ' NOT NULL',
            'model' => Schema::TYPE_STRING,
            'elem_id' => Schema::TYPE_INTEGER,
			'title' => Schema::TYPE_STRING,
			'ext' => Schema::TYPE_STRING,
			'size' => Schema::TYPE_INTEGER,
			'active' => Schema::TYPE_INTEGER,
			'priority' => Schema::TYPE_INTEGER,
		]);
    }

    public function down()
    {
		$this->dropTable('{{%gallery}}');
    }
}
