<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_104907_devsection_pp_upd extends Migration
{
    public function up()
    {
		$this->alterColumn( 'devsection', 'task', Schema::TYPE_TEXT);
		$this->addColumn('projectparam', 'position',Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->alterColumn( 'devsection', 'task', Schema::TYPE_STRING);
		$this->dropColumn('projectparam','position');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
