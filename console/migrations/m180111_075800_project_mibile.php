<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180111_075800_project_mibile
 */
class m180111_075800_project_mibile extends Migration
{
  public function up()
  {
    $this->addColumn('projectparam', 'desk', Schema::TYPE_INTEGER);
    $this->addColumn('projectparam', 'table', Schema::TYPE_INTEGER);
    $this->addColumn('projectparam', 'phone', Schema::TYPE_INTEGER);
  }

  public function down()
  {
    $this->addColumn('projectparam', 'desk', Schema::TYPE_INTEGER);
    $this->addColumn('projectparam', 'table', Schema::TYPE_INTEGER);
    $this->addColumn('projectparam', 'phone', Schema::TYPE_INTEGER);
  }
}
