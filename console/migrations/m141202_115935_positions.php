<?php

use yii\db\Schema;
use yii\db\Migration;

class m141202_115935_positions extends Migration
{
    public function up()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

		$this->createTable('{{%position}}', [
            'id' => Schema::TYPE_PK,
			'project_id'=> Schema::TYPE_INTEGER . ' NOT NULL',
			'ap_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'query'=> Schema::TYPE_STRING . ' NOT NULL',
			'wordstat' => Schema::TYPE_INTEGER . ' NOT NULL',
            'position' => Schema::TYPE_INTEGER . ' NOT NULL',
			'prev_position' => Schema::TYPE_INTEGER . ' NOT NULL',
			'weight' => Schema::TYPE_FLOAT . ' NOT NULL',
        ], $tableOptions);
        //$this->createIndex('alias', '{{%position}}', 'alias', true);
    }

    public function down()
    {
        $this->dropTable('{{%position}}');
    }
}
