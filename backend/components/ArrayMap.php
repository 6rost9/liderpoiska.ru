<?php

namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;

class ArrayMap extends ArrayHelper {
    public static function multilvl($array, $from, $to, $group = null)
    {
        $result = [];
        foreach ($array as $element) {
			$r = static::multilvl($element->sections, 'id', 'name');
            $key = static::getValue1($element, $from);
            $value = static::getValue1($element, $to);
            if ($group !== null) {
                $result[static::getValue1($element, $group)][$key] = $value;
            } else {
                $result[$key] = $value;
				foreach($r as $k => $v) {
					$result[$k] = $v;
				}
            }
        }

        return $result;
    }
	
	public static function multilvlTpl($array)
    {
        $result = [];
        foreach ($array as $element) {
			if(is_array($element)) {
				$r = static::multilvlTpl($element);
            } else $result[$element] = $element;
			if (!empty($r)) {
				foreach($r as $k => $v) {
					$result[$k] = $v;
				}
			}
        }

        return $result;
    }
	
	public static function getValue1($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }

        if (is_array($array) && array_key_exists($key, $array)) {
            return $array[$key];
        }

        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue1($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }

        if (is_object($array)) {
            return $array->$key;
        } elseif (is_array($array)) {
            return array_key_exists($key, $array) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }
	
}
