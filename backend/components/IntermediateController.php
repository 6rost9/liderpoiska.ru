<?php

namespace backend\components;

use Yii;
use yii\web\Controller;

class IntermediateController extends Controller {
	public $seoTitle;
	public $seoDesc;
	public $seoKw;
	public $pageName;
	//public $name;
}
