<?php

namespace app\components;

use Yii;

class Declension {
    public static function click($num) {
		$ost=$num%10;
		if ($ost!=0) {
			switch ($ost) {
				case 1:	$word='переход';	break;
				case 2:
				case 3:
				case 4: $word='перехода'; break;
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:	$word='переходов'; break;
			}
		} else $word='переходов';
		return $num." ".$word;
	}
	public static function rub($num) {
		$ost=$num%10;
		if ($ost!=0) {
			switch ($ost) {
				case 1:	$word='рубль';	break;
				case 2:
				case 3:
				case 4: $word='рубля'; break;
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:	$word='рублей'; break;
			}
		} else $word='рублей';
		return $num." ".$word;
	}
}
