$('document').ready(function() {

	$("#projectparam-bg_color").spectrum({
		preferredFormat: "hex",
		showInput: true,
	});
	console.log('start admin.js');
	$(document).on('filebatchselected', '[data-krajee-fileinput]', function(event, files) {
		console.log('drop');
		$(this).fileinput('upload');
	});

/*
	$("[name=file]").on('filepreajax', function(event, previewId, index) {
		console.log('2323232');
	});

	$('.file-input, #w1, #w0, input').on('filepreajax', function(event, data, previewId, index) {
		console.log('2323232');
	});

	$input = $("[name=file]");
	console.log($input);
	$input.fileinput({
		uploadUrl:'/gallery/file-upload/',
		maxFileCount: 5
	}).on("filebatchselected", function(event, files) {
		console.log(files);
		setTimeout(function() {
			$input.fileinput("upload");
		}, 1500);
	});
*/
	$('body').on('click', '.gallery_upd', function() {
		$btn = $(this);
		title = $(this).parents('.file-thumbnail-footer').find('[name=gallery_title]').val();
		url = '/gallery/update/?id='+$(this).parents('.file-thumbnail-footer').find('[data-key]').attr('data-key');
		$.ajax({
			type: 'post',
			url: url,
			data: {title: title, _csrf: $('[name=_csrf]').val()},
			success: function(response) {
				$btn.find('i').removeClass('text-danger').removeClass('text-success');
				if (response.err == 0) {
					$btn.find('i').addClass('text-success');
				} else {
					$btn.find('i').addClass('text-danger');
				}
				console.log(response);
			}
		});
	});
	// console.log(new Date('2015-05-05'));
	$('[name="Projectparam[release]"]').pickmeup({
		format: 'm-Y',
		select_day: false,
		hide_on_select: true,
		default_date: false,
		locale: {
			days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
			daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
			daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
			months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'December'],
			monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
		}
	});
	var el = document.getElementById('items');
	var sortable = Sortable.create(el,{
		onEnd: function (/**Event*/evt) {
			var ds = parseInt($(evt.item).data('devsection-id'));
			var sort = parseInt(evt.newIndex+1);
			$.ajax({
			 url: '/devsection/ajax-resort?section_id='+ds+'&sort='+sort,
			 type: 'post',
			 success: function (data) {

			 },
			 error: function (data) {

			 }
			});
		},
	});

	$('.delete').on('click', function() {
		var check = confirm($(this).data('confirm'));
		elem = $(this).parent('li');
		if (check) {

			$.ajax({
			 url: $(this).data('url'),
			 type: 'post',
			 success: function (data) {
				 if (data == true) {
					 elem.fadeOut();
				 }
			 }
		 });
		} else {
			return false;
		}

	})




});
/*
var uploadID = "";
$(document).ready(function() {
    $("[name=file]").fileinput({
        uploadAsync: true,
        uploadUrl: "/gallery/file-upload/",
        uploadExtraData: function() {
                return uploadID;
            }
        });

    $("[name=file]").on('filepreajax', function(event, previewId, index) {
        uploadID = {"id": $(this).attr("id")};
    });
});
*/
