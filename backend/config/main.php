<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'view' => [
                  'class' => 'yii\web\View',
                  'renderers' => [
      				//'layout'=>false,
                      'tpl' => [
                          'class' => 'yii\smarty\ViewRenderer',
                          //'cachePath' => '@runtime/Smarty/cache',
                      ],
                      'twig' => [
                          'class' => 'yii\twig\ViewRenderer',
                          'cachePath' => '@runtime/Twig/cache',
                          // Array of twig options:
                          'options' => ['auto_reload' => true],
                          'globals' => [
      						'html' => '\yii\helpers\Html',
      						'name' => 'Carsten',
      						'GridView' => '\yii\grid\GridView',
      						'ListView' => '\yii\widgets\ListView',
      						'EasyThumbnailImage' => 'himiklab\thumbnail\EasyThumbnailImage',
      						'Url' => 'yii\helpers\Url',
      						'decl' => 'app\components\Declension',
      						//'decl' => 'yii\modules\Declension',
      					],
                      ],
                      // ...
                  ],
              ],
        'urlManager' => [
                'class'=>'yii\web\UrlManager',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'rules' => [
            				'dashboard' => 'site/index',
            				//'<action:(seo|about|context|yandex|canpromote|portfolio|conversion|dev)>' => 'site/<action>',
            				'seo/yandex' => 'site/yandex',
            				'seo/canpromote' => 'site/canpromote',
            				'seo/portfolio' => 'site/portfolio',
            				//'admin/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            				'dev/<id:\w+>' => 'site/devshow',
                        ],
              ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [	'class' => 'yii\debug\Module',
									'allowedIPs' => ['127.0.0.1', '::1', '192.168.50.1', '192.168.6.42'],];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ];
}

return $config;
