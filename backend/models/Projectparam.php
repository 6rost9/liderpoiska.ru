<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "param".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $value

 */
class Projectparam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projectparam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'project_id', 'project_type_id', 'bg_color', 'teaser'], 'required'],
            [['name', 'project_id', 'img_header', 'release', 'teaser','bg_image','logo'], 'string', 'max' => 255],
      			[['task', 'desc_header', 'after_dev', 'bg_color'], 'string'],
      			[['position', 'active', 'teaser_white', 'teaser_sub', 'project_type_id', 'desk', 'table', 'phone'], 'integer'],
            [['img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название в шапке',
            'after_dev' => 'Текст после блоков',//alias
            'task' => 'Задача',//value
            'project_id' => 'Проект',
            // 'img' => 'Картинка листинга',
            'desc_header' => 'Описание проекта в шапке',//default
            'img_header' => 'Картинка в шапке',//group_name
            'position' => 'Порядок сортировки',
            'release' => 'Релиз',
            'active' => 'Активен?',
            'logo' => 'Логотип компании',
            'bg_image' => 'Фоновая картинка',
            'teaser' => 'Тизер',
            'bg_color' => 'Цвет подложки',
            'teaser_white' => 'Белый текст в тизере',
            'teaser_sub' => 'Белая подложка под тизером',
            'project_type_id' => 'Тип проекта',
            'desk' => 'Desktop',
            'table' => 'Tablet',
            'phone' => 'Phone',
        ];
    }

  public function getProject()
  {
      return $this->hasOne(Project::className(), ['id' => 'project_id']);
  }
	public function getProjects()
  {
      return $this->hasOne(Project::className(), ['id' => 'project_id']);
  }
  public function getDevsections()
  {
      return $this->hasMany(Devsection::className(), ['projectparam_id' => 'id']);
  }
}
