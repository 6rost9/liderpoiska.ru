<?php

namespace backend\models;

use Yii;
//use app\models\Projectparam;
//use app\models\Position;
//use app\models\Region;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property string $short_desc
 * @property integer $region
 * @property integer $startdate
 * @property integer $active
 * @property string $img
 * @property integer $show_in_slider
 * @property integer $dev
 * @property integer $ap_id
 * @property integer $cy
 * @property integer $pr
 * @property integer $top3
 * @property integer $top10
 * @property integer $top30
 * @property string $date
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'region'], 'required'],
            [['description', 'short_desc'], 'string'],
            [['region', 'startdate', 'active', 'show_in_slider', 'dev', 'ap_id', 'cy', 'pr', 'top3', 'top10', 'top30'], 'integer'],
            [['date'], 'safe'],
            [['name', 'url'], 'string', 'max' => 255],
            [['url'], 'unique'],
			[['img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
            'short_desc' => 'Short Desc',
            'region' => 'Region',
            'startdate' => 'Startdate',
            'active' => 'Active',
            'img' => 'Img',
            'show_in_slider' => 'Show In Slider',
            'dev' => 'Dev',
            'ap_id' => 'Ap ID',
            'cy' => 'Cy',
            'pr' => 'Pr',
            'top3' => 'Top3',
            'top10' => 'Top10',
            'top30' => 'Top30',
            'date' => 'Date',
        ];
    }

	public function getProjectparams()
    {
        return $this->hasMany(Projectparam::className(), ['project_id' => 'id']);
    }
	public function getPositions()
    {
        return $this->hasMany(Position::className(), ['project_id' => 'id']);
    }
	public function getRegions()
    {
        return $this->hasOne(Region::className(), ['id' => 'region']);
    }
	public function getDevsections()
    {
        return $this->hasMany(Devsection::className(), ['project_id' => 'id']);
    }
  public function getSlider()
    {
        return $this->hasOne(Slider::className(), ['project_id' => 'id']);
    }
}
