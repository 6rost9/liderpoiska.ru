<?php

namespace backend\models;

use Yii;
use backend\models\Project;
/**
 * This is the model class for table "region".
 *
 * @property integer $id
 * @property string $name
 * @property integer $show_in_slider
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['show_in_slider', 'sorting'], 'integer'],
            [['name', 'region_full'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'show_in_slider' => 'Show In Slider',
			'region_full' => 'Название региона(области)',
			'sorting' => 'Сортировка',
        ];
    }
	public function getProjects()
    {
        return $this->hasMany(Project::className(), ['region' => 'id']);
    }
}
