<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $period_1
 * @property integer $val_1
 * @property string $period_2
 * @property integer $val_2
 * @property string $period_3
 * @property integer $val_3
 * @property string $period_4
 * @property integer $val_4
 * @property string $period_5
 * @property integer $val_5
 * @property integer $per_day
 * @property integer $per_month
 * @property string $img
 * @property string $param
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id'], 'required'],
            [['project_id', 'val_1', 'val_2', 'val_3', 'val_4', 'val_5'], 'integer'],
            [['period_1', 'period_2', 'period_3', 'period_4', 'period_5', 'param', 'note_top', 'note_bottom'], 'string', 'max' => 255],
			[['img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'period_1' => 'Дата 1',
            'val_1' => 'Значение 1',
            'period_2' => 'Дата 2',
            'val_2' => 'Значение 2',
            'period_3' => 'Дата 3',
            'val_3' => 'Значение 3',
            'period_4' => 'Дата 4',
            'val_4' => 'Значение 4',
            'period_5' => 'Дата 5',
            'val_5' => 'Значение 5',
            'note_top' => 'Верхняя заметка (span, strong)',
            'note_bottom' => 'Нижняя заметка (span, strong)',
            'img' => 'Картинка',
            'param' => 'Сортировка',
        ];
    }
    public function getProject()
      {
          return $this->hasOne(Project::className(), ['id' => 'project_id']);
      }
}
