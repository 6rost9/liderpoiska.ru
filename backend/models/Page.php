<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property string $short_desc
 * @property string $content
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $parent
 * @property integer $active
 * @property string $url
 * @property string $img
 * @property string $video
 * @property string $bg_head
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['description', 'short_desc', 'content', 'seo_description'], 'string'],
            [['parent', 'active', 'sort'], 'integer'],
            [['name', 'alias', 'seo_title', 'seo_keywords', 'url', 'img', 'bg_head'], 'string', 'max' => 255],
            [['alias'], 'unique'],
			[['video'], 'file', 'extensions' => 'mp4, webm', 'mimeTypes' => ['video/mp4', 'video/webm']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'description' => 'Описание',
            'short_desc' => 'Пункт меню',
            'content' => 'Content',
            'seo_title' => 'Seo Title',
            'seo_description' => 'Seo Description',
            'seo_keywords' => 'Seo Keywords',
            'parent' => 'Parent',
            'active' => 'Показать в меню',
            'url' => 'Url',
            'img' => 'Img',
            'video' => 'Видео',
            'bg_head' => 'Картинка шапки',
            'sort' => 'Сортировка'
        ];
    }
	public function getSections()
  {
      return $this->hasMany(Section::className(), ['parent' => 'alias']);
  }
  public function getParentinfo()
  {
      return $this->hasOne(Page::className(), ['id' => 'parent']);
  }
}
