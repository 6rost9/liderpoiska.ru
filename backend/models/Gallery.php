<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $path
 * @property string $model
 * @property integer $elem_id
 * @property string $title
 * @property string $ext
 * @property integer $size
 * @property integer $active
 * @property integer $priority
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'required'],
            [['elem_id', 'size', 'active', 'priority'], 'integer'],
            [['path', 'model', 'title', 'ext'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'model' => 'Model',
            'elem_id' => 'Elem ID',
            'title' => 'Title',
            'ext' => 'Ext',
            'size' => 'Size',
            'active' => 'Active',
            'priority' => 'Priority',
        ];
    }
}
