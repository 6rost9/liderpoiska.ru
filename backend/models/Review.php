<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "review".
 *
 * @property integer $id
 * @property string $reviewer
 * @property string $rank
 * @property string $project_id
 * @property string $review_text
 * @property string $img
 */
class Review extends \yii\db\ActiveRecord
{
	private $img;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['img'], 'safe'],
            [['reviewer', 'rank', 'project_id'], 'required'],
            [['review_text'], 'string'],
            [['reviewer', 'rank', 'project_id'], 'string', 'max' => 255],
			[['img'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reviewer' => 'Reviewer',
            'rank' => 'Rank',
            'project_id' => 'Project ID',
            'review_text' => 'Review Text',
            'img' => 'Img',
        ];
    }
	public function getProjects()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
