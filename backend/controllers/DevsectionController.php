<?php

namespace backend\controllers;

use Yii;
use backend\models\Devsection;
use backend\models\Projectparam;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DevsectionController implements the CRUD actions for Devsection model.
 */
class DevsectionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Devsection models.
     * @return mixed
     */
    public function actionIndex()
    {
		if (isset($_GET['sort'])) {
			if (strpos($_GET['sort'], '-') !== false) $sort = str_replace('-', '', $_GET['sort']).' DESC ,';
			else $sort = $_GET['sort'].' ASC ,';
		}
		else $sort = '';
        $dataProvider = new ActiveDataProvider([
            'query' => Devsection::find()->orderby("$sort projectparam_id, position"),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Devsection model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Devsection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $model = new Devsection();

      if ($model->load(Yii::$app->request->post())) {
        $file = UploadedFile::getInstance($model, 'img');
        if ($file) {
          $model->img = date('d-m-y-G-i-s').'_'.$file->name;
          $dir = Yii::getAlias(__DIR__ .'/../../frontend/web/uploads/img/');
          $uploaded = $file->saveAs( $dir . $model->img);
        }
        if ($model->save()) {
          $this->reSort($model);
          return $this->redirect(['update', 'id' => $model->id]);
        } else echo 'no save';
      } else {
        return $this->render('create', [
          'model' => $model,
          'params' => Projectparam::find()->all(),
          'tpls' => $this->tpls()
        ]);
      }
    }

    /**
     * Updates an existing Devsection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$oldImg = $model->img;

        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$model->img = date('d-m-y-G-i-s').'_'.$file->name;
				$dir = Yii::getAlias(__DIR__ .'/../../frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img);
			} else $model->img = $oldImg;

			if ($model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			} else echo 'no save';

        } else {
            return $this->render('update', [
                'model' => $model,
				'params' => Projectparam::find()->all(),
				'tpls' => $this->tpls()
            ]);
        }
    }

    /**
     * Deletes an existing Devsection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->isAjax) {
          return true;
        } else {
          return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Devsection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Devsection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Devsection::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function tpls() {
		$dir = dirname(dirname(__DIR__))."/frontend/views/devsections";
		$files = scandir($dir);
		foreach($files as $item) {
			if ($item != "." && $item != "..") {
				$tpls["$item"] = $item;
			}
		}
		return $tpls;
	}

	public function actionFileDelete() {
		$model = Devsection::findOne($_POST['key']);
		$file = $model->img;
		$model->img = '';
		$del = $model->save();
		$unlink = unlink(Yii::getAlias('@webroot/../../frontend/web/uploads/img/').$file);

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];

		return $resp;
    }

  public function reSort($pagesection) {
    $pagesections = Devsection::find()
      ->where(['projectparam_id' => $pagesection->projectparam_id])
      ->orderby('position')
      ->all();
    $i=1;

    echo $pagesection->position;

    foreach($pagesections as $ps) {
      if ($ps->id != $pagesection->id) {
        $pos = ((int)$i < (int)$pagesection->position || (int)$pagesection->position == 0) ? $i : $i+1;
        $ps->position = $pos;
        $ps->save();
        $i++;
      } else {
        $found_obj = $ps;
      }

    }
    $found_obj->position = $pagesection->position == 0 ? $i : $pagesection->position;
    $found_obj->save();
  }

  public function actionAjaxResort($section_id, $sort)
  {
    $pagesection = Devsection::findOne($section_id);
    $pagesection->position = $sort;
    // $pagesection->save();
    return $this->reSort($pagesection);
    // return $pagesection->position;

  }
}
