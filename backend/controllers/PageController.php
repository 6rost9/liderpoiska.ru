<?php

namespace backend\controllers;

use Yii;
use backend\models\Page;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
//use backend\models\UploadForm;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Page::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new Page();
		$dirVid = Yii::getAlias(__DIR__ .'/../../web/uploads/video/');
		$dirImg = Yii::getAlias(__DIR__ .'/../../web/uploads/img/');

       if ($model->load(Yii::$app->request->post())) {

			$fileVid = UploadedFile::getInstance($model, 'video');
			$fileImg = UploadedFile::getInstance($model, 'bg_head');
			if ($fileImg) {
				$model->bg_head = $fileImg->name;
				$uploadedImg = $fileImg->saveAs( $dirImg . $model->bg_head);
			}
			if ($fileVid) {
				$model->video = $fileVid->name;
				$uploadedVid = $fileVid->saveAs( $dirVid . $model->video);
			}
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$oldBgHead = $model->bg_head;
		$oldVideo = $model->video;
		if ($model->load(Yii::$app->request->post())) {

			$fileVid = UploadedFile::getInstance($model, 'video');
			$fileImg = UploadedFile::getInstance($model, 'bg_head');
			if ($fileImg) {
				$model->bg_head = $fileImg->name;
				$dirImg = Yii::getAlias(__DIR__ .'/../../web/uploads/img/');
				$uploadedImg = $fileImg->saveAs( $dirImg . $model->bg_head);
			} else $model->bg_head = $oldBgHead;
			if ($fileVid) {
				$model->video = $fileVid->name;
				$dirVid = Yii::getAlias(__DIR__ .'/../../web/uploads/video/');
				$uploadedVid = $fileVid->saveAs( $dirVid . $model->video);
			} else $model->video = $oldVideo;
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
