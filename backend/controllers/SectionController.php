<?php

namespace backend\controllers;

use Yii;
use backend\models\Section;
use backend\models\Page;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * SectionController implements the CRUD actions for Section model.
 */
class SectionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Section models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Section::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Section model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

    }

    /**
     * Creates a new Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Section();

		$dir = __DIR__."/../../frontend/views/sections";

		$l1 = scandir($dir);
		foreach($l1 as $item1) {
			if ($item1 != "." && $item1 != "..") {
				if(is_dir("$dir/$item1")) {
					$l2 = scandir("$dir/$item1");
					foreach($l2 as $item2) {
						if ($item2 != "." && $item2 != "..") {
							if(is_dir("$dir/$item1/$item2")) {
								$l3 = scandir("$dir/$item1/$item2");
								foreach($l3 as $item3) {
									if ($item3 != "." && $item3 != "..") {
										if(is_dir("$dir/$item1/$item2/$item3")) {
											$l4 = scandir("$dir/$item1/$item2/$item3");
											foreach($l4 as $item4) {
												if ($item4 != "." && $item4 != "..") {
													if(is_dir("$dir/$item1/$item2/$item3/$item4")) {

													} else {
														$tpls[$item1][$item2][$item3][$item4] = $item4;
													}
												}
											}

										} else {
											$tpls[$item1][$item2][$item3] = $item3;
										}
									}

								}
							} else {
								$tpls[$item1][$item2] = $item2;
							}
						}
					}
				} else {
					$tpls["$item1"] = $item1;
				}
			}
		}

		$parent = Page::find()->with('sections')->all();

		echo"<pre>";
		//print_r($parent);
		echo"</pre>";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'tpls' => $tpls,
				'parent' => $parent,
            ]);
        }
    }

    /**
     * Updates an existing Section model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		$dir = __DIR__."/../../frontend/views/sections";
		$l1 = scandir($dir);
		foreach($l1 as $item1) {
			if ($item1 != "." && $item1 != "..") {
				if(is_dir("$dir/$item1")) {
					$l2 = scandir("$dir/$item1");
					foreach($l2 as $item2) {
						if ($item2 != "." && $item2 != "..") {
							if(is_dir("$dir/$item1/$item2")) {
								$l3 = scandir("$dir/$item1/$item2");
								foreach($l3 as $item3) {
									if ($item3 != "." && $item3 != "..") {
										if(is_dir("$dir/$item1/$item2/$item3")) {
											$l4 = scandir("$dir/$item1/$item2/$item3");
											foreach($l4 as $item4) {
												if ($item4 != "." && $item4 != "..") {
													if(is_dir("$dir/$item1/$item2/$item3/$item4")) {

													} else {
														$tpls[$item1][$item2][$item3][$item4] = $item4;
													}
												}
											}

										} else {
											$tpls[$item1][$item2][$item3] = $item3;
										}
									}

								}
							} else {
								$tpls[$item1][$item2] = $item2;
							}
						}
					}
				} else {
					$tpls["$item1"] = $item1;
				}
			}
		}

		$parent = Page::find()->with('sections')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'tpls' => $tpls,
				'parent' => $parent,
            ]);
        }

    }

    /**
     * Deletes an existing Section model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Section model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Section the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Section::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
