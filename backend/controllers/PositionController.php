<?php

namespace backend\controllers;

use Yii;
use backend\models\Position;
use backend\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\AllPositions;


/**
 * PositionController implements the CRUD actions for Position model.
 */
class PositionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Position models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Position::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Position model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Position model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Position();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Position model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Position model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Position model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Position the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Position::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionGetpositions()
    {

		$api = new AllPositions('2f1cc568373cedcc63d17a1938cf6221');
		echo "<pre>";

		$projects = Project::find()->where("active='1'")->where("date<  DATE_SUB(NOW(), INTERVAL 30 MINUTE)")->limit(20)->all();
		//$i=0;
		$k=[1=>1,
			2=>1,
			3=>1,
			4=>.85,
			5=>.6,
			6=>.5,
			7=>.5,
			8=>.3,
			9=>.3,
			10=>.2];
		foreach ($projects as $project) {
			//echo $project->date;continue;
			//if ($i<=5) {//$project['id_project'] <= 130609
				$report = $api->get_report($project->ap_id);

				$project->top3 = $report['top3'];
				$project->top10 = $report['top10'];
				$project->top30 = $report['top30'];
				//$project->date = date("Y-m-d H:i:s");
				$project->save();
				//print_R($project);
				echo "обновил " . $project->url. "  <br>";

				$sengines = is_array($report['sengines']) ? $report['sengines'] : array($report['sengines']);
				$id_se = false;
				foreach ($sengines as $seng) {
					if ($seng['name_se'] == 'Яндекс') {
						$id_se = $seng['id_se'];
					}
				}
				$queries = is_array($report['queries']) ? $report['queries'] : array($report['queries']);

				if ($id_se) {
				foreach ($queries as $query) {
					$pos_id = $id_se."_".$query['id_query'];
					if (($model = Position::find()->where("ap_id='$query[id_query]'")->one()) !== null) {
						$model->wordstat = $query['wordstat'];
						$model->position = $report['positions']["$pos_id"]['position'];
						$model->prev_position = $report['positions']["$pos_id"]['prev_position'];
						if ($model->position < 11 && $model->position > 0) {
							$model->weight = $k[$model->position] * $model->wordstat;
						} else $model->weight = 0;
						$model->save();
					} else {
						$model = new Position();
						$model->project_id = $project->id;
						$model->ap_id = $query['id_query'];
						$model->query = $query['query'];
						$model->wordstat = $query['wordstat'];
						//$pos = $report['positions']["$pos_id"]['position'];
						if (!$report['positions']["$pos_id"]['position']) continue;
						$model->position = $report['positions']["$pos_id"]['position'];
						$model->prev_position = $report['positions']["$pos_id"]['prev_position'];
						if ($model->position < 11 && $model->position > 0) {
							$model->weight = $k[$model->position] * $model->wordstat;
						} else $model->weight = 0;
						$model->save();
					}
				}
			}
		//	} $i++;
		}
		/*print_r(
			$api->get_projects()
			//$api->get_report(184855)
		);*/
		//print_r($api->get_projects_group());

		//$model = new Position();
		//$model = $this->findModel(1);
		//print_R($model);
		//$model->save();

    }
}
