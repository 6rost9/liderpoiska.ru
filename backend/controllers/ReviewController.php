<?php

namespace backend\controllers;

use Yii;
use backend\models\Review;
use backend\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\UploadForm;
use yii\web\UploadedFile;
/**
 * ReviewController implements the CRUD actions for Review model.
 */
class ReviewController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Review models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Review::find(),
			'pagination'=>['pageSize'=>9]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Review model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Review model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

	public function actionCreate()
    {
		$model = new Review();
        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$path_parts = pathinfo($file->name);
				$newname = date('d-m-y-G-i-s').'-'.$path_parts['filename'].'.'.$path_parts['extension'];
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$model->img = $newname;
				$uploaded = $file->saveAs( $dir . $model->img);
			}

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';
        } else {
            return $this->render('create', [
                'model' => $model,
				'project' => Project::find()->all(),
            ]);
        }
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$img = $model->img;
        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$path_parts = pathinfo($file->name);
				$newname = date('d-m-y-G-i-s').'-'.$path_parts['filename'].'.'.$path_parts['extension'];
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$model->img = $newname;
				$uploaded = $file->saveAs( $dir . $model->img);
			} else $model->img = $img;

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';
        } else {
            return $this->render('update', [
                'model' => $model,
				'project' => Project::find()->all(),
            ]);
        }
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
