<?php

namespace backend\controllers;

use Yii;
use backend\models\Gallery;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use himiklab\thumbnail\EasyThumbnailImage;
/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
			'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Gallery::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Gallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Gallery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

		$model->title = $_POST['title'];
		if ($model->save()) {
			$err = 0;
		} else {
			$err = 1;
        }
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return ['err' => $err];
    }

    /**
     * Deletes an existing Gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Gallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Gallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionFileUpload()
    {
		$model = new Gallery;
		//Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		//return ['error'=>serialize($_POST)];
		$copydir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
		$dir = Yii::getAlias('uploads/img/');
		$new_name = date('d-m-Y_H-i-s_').$_FILES['file']['name'];
		$filepath = $dir . $new_name;
		$copypath = $copydir . $new_name;
		$model->path = $filepath;

		$model->model = $_POST['model'];
		$model->elem_id = $_POST['elem_id'];
		//$model->title = $_POST['title'];
		$model->active = 1;
		$ext = explode('.', $_FILES['file']['name']);
		$model->ext = $ext[1];
		$model->size = (string)$_FILES['file']['size'];
		//print_r($model); exit;

		$save = $model->save();
		if ($save && $_FILES['file']['name']!='') {
			$copy = copy($_FILES['file']['tmp_name'], $copypath);
		} else $copy = false;
		$err = serialize($_POST);
		$img = EasyThumbnailImage::thumbnailFileUrl(
			Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/').$filepath,
			200,
			150,
			'inset'
		);
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($copy && $save)
		? ['initialPreview' => ["<img src='$img' class='file-preview-image'>"],
		'initialPreviewConfig' => [['caption' => "$new_name", 'width' => '120px', 'url' => '/gallery/file-delete/', 'key' => $model->id, 'extra' => ['model' => $_POST['model'], 'elem_id'=>$_POST['elem_id']]]],
		'append' => true]
		: ['error' => 'Ошибка загрузки'];
		return $resp;
    }

	public function actionFileDelete()
    {
		$model = Gallery::findOne($_POST['key']);
		$file = $model->path;
		$del = $model->delete();
		$path = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/').$file;
		$unlink = unlink($path);

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];
		return $resp;
    }

}
