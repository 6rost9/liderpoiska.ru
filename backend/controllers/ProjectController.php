<?php

namespace backend\controllers;

use Yii;
use backend\models\Project;
use backend\models\Region;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\UploadForm;
use yii\web\UploadedFile;
use backend\components\AllPositions;
/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Project::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
/*    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
*/
	public function actionCreate()
    {
		$model = new Project();

        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$path_parts = pathinfo($file->name);
				$newname = date('d-m-y-G-i-s').'-'.$path_parts['filename'].'.'.$path_parts['extension'];
				$dir = Yii::getAlias(__DIR__ .'/../../web/uploads/img/logo/');
				$model->img = $newname;
				$uploaded = $file->saveAs( $dir . $model->img);
			}

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';

        } else {
            return $this->render('create', [
                'model' => $model,
				'region' => Region::find()->all(),
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$img = $model->img;
        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$path_parts = pathinfo($file->name);
				$newname = date('d-m-y-G-i-s').'-'.$path_parts['filename'].'.'.$path_parts['extension'];
				$dir = Yii::getAlias(__DIR__ .'/../../web/uploads/img/logo/');
				$model->img = $newname;
				$uploaded = $file->saveAs( $dir . $model->img);
			} else $model->img = $img;

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';
        } else {
            return $this->render('create', [
                'model' => $model,
				'region' => Region::find()->all(),
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionProjectsap()
    {

		$api = new AllPositions('2f1cc568373cedcc63d17a1938cf6221');

		/*echo "<pre>";
		print_r(
			$api->get_projects()
		);
		echo "</pre>";*/
		$prjcts = $api->get_projects();
		foreach ($prjcts as $k => $v) {
			if (($model = Project::find()->where("url='$v[url]'")->one()) !== null) {

				$model->ap_id = $v['id_project'];
				$model->cy = $v['cy'];
				$model->pr = $v['pr'];
				$model->active = 1 - $v['sleep'];
				$model->save();
				echo "обновил $v[url] <br>";

			} else {
				$model = new Project();
				$model->name = $v['url'];
				$model->url = $v['url'];
				$model->ap_id = $v['id_project'];
				$model->cy = $v['cy'];
				$model->pr = $v['pr'];
				$model->active = 1 - $v['sleep'];
				$model->region = 0;
				$s = $model->save();

				echo "создал $v[url] <br>";
			}
		}
		//return $this->redirect(['index']);
    }

	public function actionSearch()
    {
		$request = Yii::$app->request->post();
		$model = Project::find()->where($request['field'] .' LIKE "%'. $request['value'] .'%"');

		if ($model) {
			$dataProvider = new ActiveDataProvider([
				'query' => $model,
				'pagination'=>['pageSize'=>1000]
			]);

			$this->layout = false;
			return $this->render('search', [
				'dataProvider' => $dataProvider,
			]);
		} else return 'Ничего не найдено';

    }
}
