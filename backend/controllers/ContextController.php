<?php

namespace backend\controllers;

use Yii;
use backend\models\Context;
use backend\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\UploadForm;
use yii\web\UploadedFile;
/**
 * ContextController implements the CRUD actions for Context model.
 */
class ContextController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Context models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Context::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Context model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	public function actionLoad($id=1)
    {
		$this->layout = false;
		if(!empty($_GET['id'])) $id=$_GET['id'];
		$model = Context::find()->where(['id'=>$id])->with('projects')->all();
		$model = $model[0];

        return $this->render('//sections/context/context_item.twig', [
            'c' => $model,
        ]);
    }

    /**
     * Creates a new Context model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate()
    {
		$model = new Context();

        if ($model->load(Yii::$app->request->post())) {

			$file_forecast = UploadedFile::getInstance($model, 'forecast_img');
			if ($file_forecast) {
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$model->forecast_img = $file_forecast->name;
				$uploaded1 = $file_forecast->saveAs( $dir . $model->forecast_img);
			}
			$file_result = UploadedFile::getInstance($model, 'result_img');
			if ($file_result) {
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$model->result_img = $file_result->name;
				$uploaded2 = $file_result->saveAs( $dir . $model->result_img);
			}

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';

        } else {
			return $this->render('create', [
				'model' => $model,
				'project' => Project::find()->all(),
			]);
		}
    }
/*    public function actionCreate()
    {
        $model = new Context();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
*/
    /**
     * Updates an existing Context model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$forecast_img = $model->forecast_img;
		$result_img = $model->result_img;
		if ($model->load(Yii::$app->request->post())) {

			$file_forecast = UploadedFile::getInstance($model, 'forecast_img');
			if ($file_forecast) {
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$model->forecast_img = $file_forecast->name;
				$uploaded1 = $file_forecast->saveAs( $dir . $model->forecast_img);
			} else $model->forecast_img = $forecast_img;
			$file_result = UploadedFile::getInstance($model, 'result_img');
			if ($file_result) {
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$model->result_img = $file_result->name;
				$uploaded2 = $file_result->saveAs( $dir . $model->result_img);
			} else $model->result_img = $result_img;

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';

        } else {
			return $this->render('create', [
				'model' => $model,
				'project' => Project::find()->all()
			]);
        }
    }

    /**
     * Deletes an existing Context model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Context model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Context the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Context::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
