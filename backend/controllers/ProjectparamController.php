<?php

namespace backend\controllers;

use Yii;
use backend\models\Projectparam;
use backend\models\Project;
use backend\models\ProjectTypes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * ParamController implements the CRUD actions for Param model.
 */
class ProjectparamController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Param models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Projectparam::find(),
            'sort'=> ['defaultOrder' => ['release' => SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Param model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Param model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate()
    {
		$model = new Projectparam();

        if ($model->load(Yii::$app->request->post())) {
			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$model->img = date('d-m-y-G-i-s').'_'.$file->name;
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img);
			}
			$file = UploadedFile::getInstance($model, 'img_header');
			if ($file) {
				$model->img_header = $file->name;
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img_header);
			}

			$model->release = (string)strtotime('01-'.$model->release);

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				echo 'no save <pre>';
				print_r($model);
			}
        } else {
            return $this->render('create', [
                'model' => $model,
								'project' => Project::find()->all(),
								'projectTypes' => ProjectTypes::find()->all(),
            ]);
        }
    }

    /**
     * Updates an existing Param model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		// $model = $this->findModel($id);
    $model = Projectparam::find()->with(['devsections'=>function ($query) {
      $query->orderBy('position');
    } ])->where(['id' => $id])->one();
		$oldImg = $model->img;
		$oldImgHeader = $model->img_header;
		$oldLogo = $model->logo;
		$oldBg = $model->bg_image;


        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$model->img = date('d-m-y-G-i-s').'_'.$file->name;
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img);
			} else $model->img = $oldImg;

			$file = UploadedFile::getInstance($model, 'img_header');
			if ($file) {
				$model->img_header = $file->name;
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img_header);
			} else $model->img_header = $oldImgHeader;

			$file = UploadedFile::getInstance($model, 'logo');
			if ($file) {
				$model->logo = $file->name;
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');

				$uploaded = $file->saveAs( $dir . $model->logo);
			} else $model->logo = $oldLogo;

			$file = UploadedFile::getInstance($model, 'bg_image');
			if ($file) {
				$model->bg_image = $file->name;
				$dir = Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/');

				$uploaded = $file->saveAs( $dir . $model->bg_image);
			} else $model->bg_image = $oldBg;

			$model->release = (string)strtotime('01-'.$model->release);

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';
        } else {
            return $this->render('update', [
                'model' => $model,
								'project' => Project::find()->all(),
								'projectTypes' => ProjectTypes::find()->all(),

            ]);
        }
    }

    /**
     * Deletes an existing Param model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Param model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Param the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projectparam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionImgDelete() {
		$model = Projectparam::findOne($_POST['key']);
		$file = $model->img;
		$model->img = '';
		$del = $model->save();
		$unlink = unlink(Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/'.$file));

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];
		return $resp;
    }
	public function actionImgHeaderDelete() {
		$model = Projectparam::findOne($_POST['key']);
		$file = $model->img_header;
		$model->img_header = '';
		$del = $model->save();
		$unlink = unlink(Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/'.$file));

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];
		return $resp;
    }
	public function actionImgLogoDelete() {
		$model = Projectparam::findOne($_POST['key']);
		$file = $model->logo;
		$model->logo = '';
		$del = $model->save();
		$unlink = unlink(Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/'.$file));

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];
		return $resp;
    }
	public function actionImgBgImageDelete() {
		$model = Projectparam::findOne($_POST['key']);
		$file = $model->bg_image;
		$model->bg_image = '';
		$del = $model->save();
		$unlink = unlink(Yii::getAlias(dirname(dirname(__DIR__)).'/frontend/web/uploads/img/'.$file));

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];
		return $resp;
		}

	public function actionUpd() {
		$projects = Project::find()
			->joinWith(['devsections'=>function($ds) {
				$ds->orderBy('devsection.position');
			}, 'projectparams'])
			->where(['and', "devsection.id > 0"])
			->indexBy('id')
			->all();//->all()//->indexBy('alias'),

		//echo "<pre>";print_r($projects);exit;
		// echo "<pre>";
		foreach ($projects as $p) {
			$task = '';
			$after_dev = '';
			$img_header = '';
			foreach ($p->devsections as $ds) {
				if (!empty($ds->img)) {
					$img_header = $ds->img;
				}
				if (!empty($ds->task)) {
					$task = $ds->task;
				}
				if (!empty($ds->after_text)) {
					$after_dev = $ds->after_text;
				}
				if (!empty($img_header) && !empty($task) && !empty($after_dev)) {
					break;
				}
			}
			foreach ($p->projectparams as $pp) {
				$pp->img_header=$img_header;
				$pp->after_dev=$after_dev;
				$pp->task=$task;
				$pp->save();
			}
			print_r($p);
		}
		exit;
		$thisProject = $projects[$id];
		$task = '';
		$after_text = '';
		$bg_head = '';
		foreach ($thisProject->devsections as $ds) {
			if (!empty($ds->img)) {
				$bg_head = $ds->img;
			}
			if (!empty($ds->task)) {
				$task = $ds->task;
			}
			if (!empty($ds->after_text)) {
				$after_text = $ds->after_text;
			}
			if (!empty($bg_head) && !empty($task) && !empty($after_text)) {
				break;
			}
		}

	}
}
