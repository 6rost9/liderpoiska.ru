<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\models\LoginForm;
use backend\models\ContactForm;

use backend\models\Review;
use backend\models\Project;
use backend\models\Section;
use backend\models\Projectparam;
use backend\models\Param;
use backend\models\Position;
use backend\models\Region;
use backend\models\Page;
use backend\models\Order;
use backend\models\Context;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\components\IntermediateController;

class SiteController extends IntermediateController
{
	//public $layoutParams = 22;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
		// $this->layout = "admin";
		if (Yii::$app->user->isGuest) {
			return $this->redirect("/site/login");
		}
		$alias="index";
		/*
		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();


		$projects = Project::find()->where("show_in_slider='1'")->with([//where("devsite=1")
			'projectparams' => function ($query) {
				$query->indexBy('alias');
			}
		])->indexBy('id')->all();//->all()//->indexBy('alias'),

		$reviews = Review::find()->orderBy('rand()')->with('projects')->all();
		$imgreviews = Review::find()->where("img<>''")->orderBy('rand()')->with('projects')->all();

		$dataProvider = new ActiveDataProvider([
            'query' => Review::find(),
        ]);
		$projectparams = new ActiveDataProvider([
            'query' => Project::find()->joinWith([
				'projectparams' => function ($query) {
					$query->indexBy('alias');
				}
			])//->all()//->indexBy('alias'),
        ]);*/
		$page = Page::find()->where("alias='$alias'")->all();
		$page = $page[0];

		$this->pageName = $page->name;

        return $this->render('index.twig');
    }

	public function actionSeo()
    {
		$url=str_replace("/", "", Url::to('@web'));
		$sec = Section::find()->where("parent='$url'")->with(['sections'=>function($query) {
			$query->orderBy('position');
		}])->all();
		$region = Region::find()->with(['projects'=>function($query) {
			$query->with(['positions'=>function($q) {
				$q->where('active=1')->where('position<11')->orderBy('(1/position*wordstat) DESC')->limit(100000);
			}])->limit(100);
		}])->orderBy('show_in_slider DESC, id')->all();
		//$pos = Position::find()->orderBy('(1/position*wordstat)')->with('projects')->all();
		$pos = Project::find()->with(['positions'=>function($q) {
			$q->where('position<11')->orderBy('(1/position*wordstat) DESC')->limit(5);
		}])->all();

		$reviews = Review::find()->orderBy('rand()')->with('projects')->all();
		$imgreviews = Review::find()->where("img<>''")->orderBy('rand()')->with('projects')->all();

		$order = new Order();

        return $this->render('seo.twig', [	'sec'			=>	$sec,
											'pos'			=>	$pos,
											'region'		=>	$region,
											'imgreviews'	=>	$imgreviews,
											'reviews'	=>	$reviews,
											'order'	=>	$order,]);
    }

	public function actionYandex()
    {
		$url=end(explode("/", Url::to('@web')));
		$sec = Section::find()->where("parent='$url'")->with('sections')->all();

		/*echo "<pre>";
		print_r($region);
		echo "</pre>";*/

        return $this->render('yandex.twig', ['sec' => $sec]);
    }

	public function actionCanpromote()
    {
		$url=end(explode("/", Url::to('@web')));
		$sec = Section::find()->where("parent='$url'")->with('sections')->all();

		$alias=array_pop(explode("/",$url));
		//echo "<pre>";
		//print_r($region);
		//echo "</pre>";

		//$page = Page::find()->where('parent="/seo"')->with('sections')->all();

		return $this->render($alias.'.twig', ['sec' => $sec]);

        //return $this->render('yandex.twig', ['sec' => $sec]);
    }
	public function actionPortfolio()
    {
		$alias=end(explode("/", Url::to('@web')));
		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();

	/*	$region = Region::find()->with(['projects'=>function($query) {
			$query->with(['positions'=>function($q) {
				$q->where('active=1')->where('position<11')->orderBy('(1/position*frequency) DESC')->limit(1000);
			}])->limit(100);
		}])->orderBy('show_in_slider DESC, id')->all();
	*/
		$project = Project::find()->where('active=1')->with(['positions'=>function($q) {
			$q->where('weight>0')->orderBy('weight DESC');
		}])->with(['regions'=>function($query) {}])->all();

		$reviews = Review::find()->orderBy('rand()')->with('projects')->all();
		$imgreviews = Review::find()->where("img<>''")->orderBy('rand()')->with('projects')->all();

		return $this->render($alias.'.twig', [	'sec' 		=> 	$sec,
												'project'	=>	$project,
												'imgreviews'=>	$imgreviews,
												'reviews'	=>	$reviews]);
    }

	public function actionContext()
    {
		$alias=end(explode("/", Url::to('@web')));
		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();

		$context = Context::find()->with('projects')->all();

		//echo "<pre>";
		//print_r($context);
		//echo "</pre>";

		return $this->render($alias.'.twig', [	'sec' => $sec,
												'context' => $context,
												]);
    }

	public function actionConversion()
    {
		$alias=end(explode("/", Url::to('@web')));

		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();


		//$context = Context::find()->with('projects')->all();

		//echo "<pre>";
		//print_r($context);
		//echo "</pre>";

		return $this->render($alias.'.twig', [	'sec' => $sec,
												//'context' => $context,
												]);
    }

	public function actionDev()
    {
		$alias=end(explode("/", Url::to('@web')));

		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();


		$projects = Project::find()->with([
			'projectparams' => function ($query) {
				$query->indexBy('alias');
			}
		])->all();//->all()//->indexBy('alias'),

		//echo "<pre>";
		//print_r($projects);
		//echo "</pre>";

		return $this->render($alias.'.twig', [	'sec' => $sec,
												'projects' => $projects,
												]);
    }
	public function actionDevshow()
    {
		$alias=end(explode("/", Url::to('@web')));
		$id=array_pop(explode("/",$alias));

		//$sec = Section::find()->where("parent='$id'")->with('sections')->all();


		$projects = Project::find()->with([//where("devsite=1")
			'projectparams' => function ($query) {
				$query->indexBy('alias');
			}
		])->indexBy('id')->all();//->all()//->indexBy('alias'),

		$thisProject = $projects[$id];
		if (count($projects)>1) {
			reset($projects);
			while ($project = current($projects)) {
				if (key($projects) == $id) {
					prev($projects);
					$prev= key($projects);
					next($projects);
					next($projects);
					$next= key($projects);
				}
				next($projects);
			}
			if (isset($prev)) {
				$prevProject = $projects[$prev];
			} else {
				end($projects);
				$prev=key($projects);
				$prevProject = $projects[$prev];
			}
			if (isset($next)) {
				$nextProject = $projects[$next];
			} else {
				reset($projects);
				$next=key($projects);
				$nextProject = $projects[$next];
			}
		}

		//echo "<pre>";
		//print_r($nextProject);
		//echo "</pre>";
		//exit;
		return $this->render('dev_example.twig', [	//'sec' => $sec,
												'thisProject' => $thisProject,
												'nextProject' => isset($nextProject) ? $nextProject : $thisProject,
												'prevProject' => isset($prevProject) ? $prevProject : $thisProject,
												]);
    }

     public function actionLogin()
    {
		// $this->layout = 'admin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
		$alias=end(explode("/", Url::to('@web')));

		$sec = Section::find()->where("parent='$alias'")->with('sections')->all();

		return $this->render($alias.'.twig', [	'sec' => $sec	]);
    }
    public function actionSay($message="Hi") {
        return $this->render('say', ['message'=>$message]);
    }
}
