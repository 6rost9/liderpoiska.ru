<?php

namespace backend\controllers;

use Yii;
use backend\models\Slider;
use backend\models\Project;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider();

        if ($model->load(Yii::$app->request->post())) {
			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$model->img = $file->name;
				$dir = Yii::getAlias(__DIR__ .'/../../frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img);
			}
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';
        } else {
            return $this->render('create', [
                'model' => $model,
				'project' => Project::find()->orderby('name')->all()
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImg = $model->img;

        if ($model->load(Yii::$app->request->post())) {

			$file = UploadedFile::getInstance($model, 'img');
			if ($file) {
				$model->img = $file->name;
				$dir = Yii::getAlias(__DIR__ .'/../../frontend/web/uploads/img/');
				$uploaded = $file->saveAs( $dir . $model->img);
			} else $model->img = $oldImg;

			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else echo 'no save';
        } else {
            return $this->render('update', [
                'model' => $model,
				'project' => Project::find()->orderby('name')->all()
            ]);
        }
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionImgDelete() {
		$model = Slider::findOne($_POST['key']);
		$file = $model->img;
		$model->img = '';
		$del = $model->save();
		$unlink = unlink(Yii::getAlias('@frontend/web').'/uploads/img/'.$file);
		
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$resp = ($unlink && $del) ? ['success' => 'Удалено'] : ['error' => 'Ошибка удаления'];
		return $resp;
    }
}
