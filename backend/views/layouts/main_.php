﻿<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAssetLider;

use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this \yii\web\View */
/* @var $content string */

use yii\widgets\ListView;

//AppAssetLider::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?php echo Yii::$app->controller->seoTitle; ?></title>
	<meta name="description" content="<?php echo Yii::$app->controller->seoDesc; ?>">
	<meta name="keywords" content="<?php echo Yii::$app->controller->seoKw; ?>">
    <meta charset="<?= Yii::$app->charset ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= Html::encode($this->title) ?></title>

    <link rel="stylesheet" href="/assets/css/master.min.css">

    <!--[if lt IE 9]>
    <script src="js/fallback/html5shiv.js"></script>
    <script src="js/fallback/respond.min.js"></script>
    <script src="js/fallback/es5-shim.min.js"></script>
    <![endif]-->
	<?php $this->head() ?>
</head>

<body>

<?php $this->beginBody() ?>
<div class="b-page">
    <header class="b-header b-header--index"  data-bg="img/tmp/cover-index.jpg">
	<?php echo Yii::$app->controller->pageName; ?>
        <div class="b-header__video">
            <video autoplay="" loop="">
                <source src="video/_1.mp4" type="video/mp4;">
            </video>
        </div>

        <div class="b-header__top">

            <div class="b-header__container">
                <div class="b-header__logo-holder">

                    <a class="b-header__logo" href=""></a>

                </div>
				
				<?php
				/*	echo Nav::widget([
						'options' => ['class' => 'b-header__menu'],
						'items' => [
							['label' => 'Продвижение сайта', 'url' => ['/seo/'], 'class'=>'menu__link'],
							['label' => 'Контекстная реклама', 'url' => ['/context/']],
							['label' => 'Увеличение конверсии', 'url' => ['/conversion/']],
							['label' => 'Разработка сайта', 'url' => ['/dev/']],
							['label' => 'О компании', 'url' => ['/about/']],
						],
					]);
				*/	//NavBar::end();
				?>
 
				
			<nav class="b-header__menu">

                    <ul class="menu">
                        <li class="menu__item"><a class="menu__link" href="#"><span>Продвижение</span> <span>сайта</span></a></li>
                        <li class="menu__item"><a class="menu__link" href="#"><span>Контекстная</span> <span>реклама</span></a></li>
                        <li class="menu__item"><a class="menu__link" href="#"><span>Увеличение</span> <span>конверсии</span></a></li>
                        <li class="menu__item"><a class="menu__link" href="#"><span>Разработка</span> <span>сайта</span></a></li>
                        <li class="menu__item"><a class="menu__link" href="#"><span>О компании</span></a></li>
                    </ul>

                </nav>

                <div class="b-header__phone">
                    <p class="phone">
                        8 (800) 543-53-41
                    </p>
                    <p class="text">
                        <span>Бесплатно</span>
                        <span>по России</span>
                    </p>
                </div>
            </div>

        </div><!-- /.b-header__nav -->
<div class="b-header__title-holder">
	<div class="b-header__title-wrapper">
		<div class="b-header__title">

			<h1>

				Мы предлагаем

				<strong>
					<span>комплексный</span>
					<span>интернет-маркетинг</span>
				</strong>

				<a href="/seo">продвижение сайтов</a>,
				<a href="/context">контекстная реклама</a>,
				<a href="/dev">разработка сайтов</a>,
				<a href="/conversion">увеличение конверсии</a>

			</h1>

		</div>
	</div>
</div>


</header><!-- /.b-header -->

		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<?= $content ?>
</div><!-- /.b-page -->

	<footer class="b-footer">
		<div class="b-footer__container">
			<a href="#" class="b-footer__logo"></a>

			<div class="b-site-map">
				<div class="b-site-map__col">
					<h2 class="b-site-map__title"><a href="/seo/">Продвижение сайта</a></h2>
					<ul class="b-site-map__list">
						<li class="list__item">— <a href="/yandex/">Новый алгоритм Яндекса</a></li>
						<li class="list__item">— <a href="/can/">Можно ли продвинуть Ваш сайт?</a></li>
						<li class="list__item">— <a href="/portfolio/">Порфолио</a></li>
					</ul>
				</div>
				<div class="b-site-map__col">
					<h2 class="b-site-map__title">Другие услуги:</h2>
					<ul class="b-site-map__list">
						<li class="list__item">— <a href="#">Контекстная реклама</a></li>
						<li class="list__item">— <a href="#">Увеличение конверсии</a></li>
						<li class="list__item">— <a href="#">Разработка сайта</a></li>
					</ul>
				</div>
				<div class="b-site-map__col">
					<h2 class="b-site-map__title"><a href="#">О компании</a></h2>
				</div>
			</div>
		</div>
	</footer><!-- /.b-footer -->
	
	<script src="/assets/js/build/production.min.js"></script>
	<script src="/assets/js/build/dev20.js"></script>
	<script src="/assets/js/build/qqna.js"></script>
	<?php //$this->registerJsFile('@app/assets/js/script.js', self::POS_READY);?>
<?php //$this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>