<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Проекты', 'url' => ['/project']],
                    ['label' => 'Регионы', 'url' => ['/region']],
					['label' => 'Страницы', 'url' => ['/page']],
					['label' => 'Разделы', 'url' => ['/section']],
					['label' => 'Отзывы', 'url' => ['/review']],
                    ['label' => 'Контекст', 'url' => ['/context']],
					['label' => 'Позиции', 'url' => ['/position']],
					['label' => 'Портфолио', 'url' => ['/projectparam']],
					['label' => 'Слайдер', 'url' => ['/slider']],
					// ['label' => 'Портфолио (разделы)', 'url' => ['/devsection']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => ['/site/login']] :
                        ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>
<?php
if (Yii::$app->user->isGuest && yii\helpers\Url::to('@web') != '/site/login') {
	return Yii::$app->controller->redirect("/site/login");
}
?>
        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>

<script>
	jQuery(document).ready(function() {
		jQuery('.filter').submit(function(e) {
			e.preventDefault();
			jQuery.ajax({
				method: 'post',
				url: '/project/search',
				data: jQuery('.filter').serialize()+'&ajx=1',
				success: function(data) {
					console.log(data);
					jQuery('.search_result').html(data);
				}
			});

		});
	});
</script>
<script src="/assets/js/jquery.pickmeup.min.js"></script>
<script src="/assets/js/spectrum.js"></script>
<link href="/css/pickmeup.css" rel="stylesheet">
<link href="/css/spectrum.css" rel="stylesheet">
<script src="/assets/js/admin.js"></script>
</body>
</html>
<?php $this->endPage() ?>
