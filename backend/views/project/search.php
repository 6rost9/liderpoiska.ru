﻿<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'url:url',
            'description',
			'short_desc',
            'region',
            // 'startdate',
            // 'active',
			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/../../web/uploads/img/logo/'.$data->img,
							100,
							50,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						return $e;
					}
				}
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
