<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

     <?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textInput() ?>
	
	<?= $form->field($model, 'short_desc')->textInput(['maxlength' => 255]) ?>

	<?= $form->field($model, 'region')->dropDownList([
		ArrayHelper::map($region, 'id', 'name')
	]) ?>
	
    <?= $form->field($model, 'startdate')->textInput() ?>

    <?php /*echo $form->field($model, 'active')->dropDownList([
        '1'=>'Активен',
        '0'=>'Неактивен'
    ]) */ ?>
	
	<?= $form->field($model, 'active')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>

	<?= $form->field($model, 'img')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
    ]); ?>
	
	<?= $form->field($model, 'show_in_slider')->checkBox(['label' => 'Показывать в слайдере', 'uncheck' => 0, 'selected' => true]) ?>
	
	<?= $form->field($model, 'dev')->checkBox(['label' => 'Разрабатывали сайт?', 'uncheck' => 0, 'selected' => true]) ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
