﻿<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>

	<?php $form = ActiveForm::begin(['options' => ['class'=>'filter']]); ?>
		<div class="form-group field-project-region required">
			<label class="control-label" for="field">Поле</label>
			<select id="field" class="form-control inline" name="field">
				<option value='id'>id</option>
				<option value='name'>name</option>
				<option value='url'>url</option>
				<option value='description'>Description</option>
			</select>
			
			<label class="control-label" for="value">Значение</label>
			<input type="text" id="value" class="form-control inline" name="value" maxlength="100">
			
			<?= Html::submitButton('Фильтр', ['class' => 'btn btn-success']) ?>
		</div>
	<?php ActiveForm::end(); ?>

    <p>
        <?= Html::a('Create Project', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
	<div class='search_result'>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'url:url',
            'description',
			'short_desc',
            'region',
            // 'startdate',
            // 'active',
			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/../../web/uploads/img/logo/'.$data->img,
							100,
							50,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						return $e;
					}
				}
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>