<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\web\UploadedFile;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

	<?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'project_id')->dropDownList([
		ArrayHelper::map($project, 'id', 'name')
	]) ?>

    <?= $form->field($model, 'period_1')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'val_1')->textInput() ?>

    <?= $form->field($model, 'period_2')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'val_2')->textInput() ?>

    <?= $form->field($model, 'period_3')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'val_3')->textInput() ?>

    <?= $form->field($model, 'period_4')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'val_4')->textInput() ?>

    <?= $form->field($model, 'period_5')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'val_5')->textInput() ?>

    <?= $form->field($model, 'note_top')->textInput() ?>

    <?= $form->field($model, 'note_bottom')->textInput() ?>

	<?php
		$host = $_SERVER['SERVER_NAME'] == 'admin.devlp.ru' ? 'devlp.ru' : 'liderpoiska.ru';
		$imgArr = explode('/', $model->img);
		echo $form->field($model, 'img', ['options' => ['class' => 'form-group']])->widget(FileInput::classname(), [
			'options' => ['accept' => 'image/*'],
			'pluginOptions' => !empty($model->img) ? [
				
				'initialPreview'=>[Html::img('http://'.$host.'/uploads/img/'.$model->img)],
				//'initialPreview'=>[Html::img(EasyThumbnailImage::thumbnailFileUrl('@frontend/web'.$model->img,200,150,'inset'))],
				'initialPreviewConfig' => [['caption' => array_pop($imgArr), 'width' => "120px", 'url' => "/slider/img-delete/", 'key' => $model->id]],
				'overwriteInitial'=>false
			] : []
		]);
	?>

    <?= $form->field($model, 'param')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
