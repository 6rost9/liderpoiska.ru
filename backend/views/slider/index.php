<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Slider', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            ['attribute' =>'project_id',
			'content'=>function($model){
                return !empty($model->project) ? $model->project->name : 'нет проекта' ;
            }],
            'period_1',
            'val_1',
            'param',
            // 'val_2',
            // 'period_3',
            // 'val_3',
            // 'period_4',
            // 'val_4',
            // 'period_5',
            // 'val_5',
            // 'per_day',
            // 'per_month',
            // 'img',
            // 'param',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
