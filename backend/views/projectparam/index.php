<?php

use yii\helpers\Html;
use yii\grid\GridView;
use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Портфолио';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить проект', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
              'attribute'=>'release',
              'label'=>'Создано',
               'format' =>  ['date', 'MM.YYYY'],// Доступные модификаторы - date:datetime:time
              'headerOptions' => ['width' => '100'],
            ],

			'projects.name',
            'name',
            // 'task',
            'after_dev',

			// 'position',

			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/../../frontend/web/uploads/img/'.$data->img,
							100,
							50,
							EasyThumbnailImage::THUMBNAIL_OUTBOUND
						);
					} catch(Exception $e) {
						//return $e;
					}
				}
			],
			[	'format' => 'image',
				'value'=>function($data) {
					try {
						return EasyThumbnailImage::thumbnailFileUrl(
							'@webroot/../../frontend/web/uploads/img/'.$data->logo,
							100,
							50,
							'inset'
						);
					} catch(Exception $e) {
						//return $e;
					}
				}
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php
  // echo "<pre>";
  // print_r($dataProvider);
  // echo "</pre>";
?>
</div>
