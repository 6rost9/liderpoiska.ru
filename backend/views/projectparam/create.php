<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Param */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Портфолио', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $model->after_dev = "<p>Срок разработки: от ___ месяцев<br/>Стоимость разработки: от ___ рублей</p>"; 
    ?>
    <?= $this->render('_form', [
        'model' => $model,
	      'project'=> $project,
        'projectTypes' => $projectTypes,
    ]) ?>

</div>
