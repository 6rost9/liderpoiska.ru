<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\web\UploadedFile;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;
/* @var $this yii\web\View
@var $model app\models\Param
@var $form yii\widgets\ActiveForm */
$this->registerJsFile('/js/Sortable.min.js',  ['position' => yii\web\View::POS_END]);
?>

<div class="param-form">
  <?php $form = ActiveForm::begin([
  'options' => ['enctype'=>'multipart/form-data']
  ]); ?>
  <ul class="nav nav-pills nav-params">
    <li class="active"><a data-toggle="pill" href="#main">Основное</a></li>
    <li><a data-toggle="pill" href="#list">Листинг</a></li>
    <li><a data-toggle="pill" href="#sections">Разделы</a></li>
  </ul>
  <div class="tab-content projectparam">
    <div id="main" class="tab-pane fade in active">
      <div class="main-first"><?= $form->field($model, 'project_id')->dropDownList([
        ArrayHelper::map($project, 'id', 'url')
        ]) ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'release')->textInput(['maxlength' => 255, 'value'=> date('m-Y', (int)$model->release > 0 ? (int)$model->release : time())]) ?>
      </div>
      <?php
      $m_img_header = explode('/', $model->img_header);
      $img_header =  array_pop($m_img_header);
      echo $form->field($model, 'img_header')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => !empty($model->img_header) ? [
          'initialPreview'=>[
            Html::img(EasyThumbnailImage::thumbnailFileUrl('@webroot/../../frontend/web/uploads/img/'.$model->img_header,200,150,'inset'))
          ],
          'initialPreviewConfig' => [['caption' => $img_header, 'width' => "120px", 'url' => "/projectparam/img-header-delete/", 'key' => $model->id]],
          'overwriteInitial'=>false
        ]  : []
      ]); ?>
      <?= $form->field($model, 'desc_header')->textarea(['rows' => 6]) ?>
      <?= $form->field($model, 'task')->textarea(['rows' => 6]) ?>
      <?php /*
      $m_img = explode('/', $model->img);
      $img = array_pop($m_img);
      echo $form->field($model, 'img')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => !empty($model->img) ? [
          'initialPreview'=>[
            Html::img(EasyThumbnailImage::thumbnailFileUrl('@webroot/../../frontend/web/uploads/img/'.$model->img,200,150,'inset'))
          ],
          'initialPreviewConfig' => [['caption' => $img, 'width' => "120px", 'url' => "/projectparam/img-delete/", 'key' => $model->id]],
          'overwriteInitial'=>false
        ]  : []
      ]); */ ?>
      <?php /* $form->field($model, 'position')->textInput() */ ?>
      <?= $form->field($model, 'after_dev')->textarea(['rows' => 6]) ?>
      <?= $form->field($model, 'active')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>
    </div>
    <div id="list" class="tab-pane fade">
      <div class="list-first"><?php
        $m_logo = explode('/', $model->logo);
        $logo =  array_pop($m_logo);
        echo $form->field($model, 'logo')->widget(FileInput::classname(), [
          'options' => ['accept' => 'image/*'],
          'pluginOptions' => !empty($model->logo) ? [
            'initialPreview'=>[
              Html::img(EasyThumbnailImage::thumbnailFileUrl('@webroot/../../frontend/web/uploads/img/'.$model->logo,200,150,'inset'))
            ],
            'initialPreviewConfig' => [['caption' => $logo, 'width' => "120px", 'url' => "/projectparam/img-logo-delete/", 'key' => $model->id]],
            'overwriteInitial'=>false
          ]  : []
        ]);
        ?>
        <?php
        $m_bg_image = explode('/', $model->bg_image);
        $bg_image =  array_pop($m_bg_image);
        echo $form->field($model, 'bg_image')->widget(FileInput::classname(), [
          'options' => ['accept' => 'image/*'],
          'pluginOptions' => !empty($model->bg_image) ? [
            'initialPreview'=>[
              Html::img(EasyThumbnailImage::thumbnailFileUrl('@webroot/../../frontend/web/uploads/img/'.$model->bg_image,200,150,'inset'))
            ],
            'initialPreviewConfig' => [['caption' => $bg_image, 'width' => "120px", 'url' => "/projectparam/img-bg-image-delete/", 'key' => $model->id]],
            'overwriteInitial'=>false
          ]  : []
        ]);
        ?>
      </div>
      <div class="list-params">
        <div class="list-param"><?= $form->field($model, 'project_type_id')->dropDownList([
            ArrayHelper::map($projectTypes, 'id', 'name')
          ]) ?>
          <?= $form->field($model, 'teaser')->textarea(['rows' => 3]) ?>
        </div>
        <div class="list-param">
          <?= $form->field($model, 'bg_color')->textInput(['class' => 'form-control bg_pic']) ?>
          <?= $form->field($model, 'teaser_white')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>
          <?= $form->field($model, 'teaser_sub')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>
        </div>
        <div class="list-param">
          <div class="adaptive"><p>Адаптивность версии:</p></div>
          <div class="adaptive-inputs">
            <?= $form->field($model, 'desk')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>
            <?= $form->field($model, 'table')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>
            <?= $form->field($model, 'phone')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>
          </div>
        </div>
      </div>
    </div>
    <div id="sections" class="tab-pane fade devsections">
      <p>Разделы</p>
      <ul class="title">
        <li>
          <span class="id">id</span>
          <span class="name">Название</span>
          <span class="tpl">Шаблон</span>
          <span class="update"></span>
          <span class="delete"></span>
        </li>
      </ul>
      <ul id="items">

        <?php
          foreach ($model->devsections as $section) {
            echo '<li data-devsection-id="'.$section['id'].'"><span class="id">'.$section['id'].'</span><span class="name">'.$section['name'].'</span><span class="tpl">'.$section['tpl'].'</span><span class="update"><a class="btn btn-info" target="_blank" href="'.Url::toRoute(['/devsection/update','id' => $section['id']]).'">Изменить</a></span><span class="delete btn btn-danger" data-confirm="Are you sure you want to delete this item?" data-url="'.Url::toRoute(['/devsection/delete','id' => $section['id']]).'">Удалить</span></li>';
          }
        ?>
      </ul>
      <a class="btn btn-success create" href="/devsection/create" target="_blank">Создать раздел</a>
    </div>
  </div>
  <?php
  // echo "<pre>";
  // print_r($model->devsections['8']);
  // echo "</pre>";
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
