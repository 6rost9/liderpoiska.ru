<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Devsection */

$this->title = 'Изменить раздел: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Портфолио разработки сайтов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="devsection-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'params' => $params,
		'tpls' => $tpls,
    ]) ?>

</div>
