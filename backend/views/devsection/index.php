<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Портфолио разработки сайтов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devsection-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Devsection', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
			'projectparam.name',
            'description:ntext',
			'position',
            'tpl',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
