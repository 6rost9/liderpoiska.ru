<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Devsection */

$this->title = 'Создать раздел';
$this->params['breadcrumbs'][] = ['label' => 'Devsections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devsection-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    $model->description = "<h2></h2>\n<p></p>";
    ?>
    <?= $this->render('_form', [
        'model' => $model,
		'params' => $params,
		'tpls' => $tpls,
    ]) ?>

</div>
