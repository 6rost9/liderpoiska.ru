<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\components\ArrayMap;
use himiklab\thumbnail\EasyThumbnailImage;
/* @var $this yii\web\View */
/* @var $model app\models\Section */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

	<?= $form->field($model, 'parent')->dropDownList([
		ArrayMap::multilvl($parent, 'alias', 'name')
	]) ?>

    <?= $form->field($model, 'img')->textInput(['maxlength' => 255]) ?>



	<?= $form->field($model, 'tpl')->dropDownList([
		ArrayMap::multilvlTpl($tpls)
	]) ?>


	<?= $form->field($model, 'add_param_1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'add_param_2')->textarea(['rows' => 6]) ?>

    <?php
if (Yii::$app->controller->action->id != 'create') {
	$preview=[];
	$previewConfig=[];
	$previewThumbTags=[];
	if (count($model->gallerys)>0) {
		foreach ($model->gallerys as $gal) {
			if (strpos($gal->path, 'uploads/img')!==false) {
				//$preview[] = "<object></object>";
        $arrGal = explode('/', $gal->path);
        $path = array_pop($arrGal);
				$preview[] =  Html::img(EasyThumbnailImage::thumbnailFileUrl('@webroot/../../frontend/web/'.$gal->path,200,150,'inset'));
				$previewConfig[] = ['caption' => $path, 'width' => "120px", 'url' => "/gallery/file-delete/", 'key' => $gal->id];
				$previewThumbTags[] = ['{TAG_VALUE}'=>$gal->title, '{TAG_CSS_NEW}'=> 'hide', '{TAG_CSS_INIT}'=> ''];
			}
		}
	}
	$footerTemplate = '
	<div class="file-thumbnail-footer">
		<div style="margin:5px 0">
			<input name="gallery_title" class="kv-input kv-init form-control input-sm {TAG_CSS_INIT}" value="{TAG_VALUE}" placeholder="Enter caption...">
		</div>
		{actions}
	</div>';
	$actions = '
	<div class="file-actions">
		<div class="file-footer-buttons">
		{upload}{delete}
		<button type="button" class="btn btn-xs btn-default gallery_upd" title="Edit file">
			<i class="glyphicon glyphicon-pencil"></i>
		</button>
		</div>
		<div class="file-upload-indicator" tabindex="-1" title="{indicatorTitle}">{indicator}</div>
		<div class="clearfix"></div>
	</div>';
	echo FileInput::widget([
		'name' => 'file',
		'options'=>[
			'multiple'=>true
		],
		'pluginOptions' => [
			'uploadUrl' => Url::to(['/gallery/file-upload/']),
			'uploadAsync' => true,
			'showUpload' => false,
			'showRemove' => false,
			'showCaption' => false,
			'showBrowse' => false,
			'layoutTemplates'=> ['footer'=>$footerTemplate, 'actions'=>$actions],
			'overwriteInitial'=> false,
			'uploadExtraData' => [
				'model' => $model->tableName(),
				'elem_id' => $model->id
			],
			/*'filepreupload'=>['data'=>['extra'=>[
				'model' => $model->tableName(),
				'elem_id' => $model->id
			]]],*/
			'maxFileCount' => 100,
			'initialPreview'=> $preview,
			'initialPreviewConfig' => $previewConfig,
			'initialPreviewThumbTags' => $previewThumbTags,
			'previewThumbTags'=> [
				'{TAG_VALUE}'=> '',        // no value
				'{TAG_CSS_NEW}'=> '',      // new thumbnail input
				'{TAG_CSS_INIT}'=> 'hide'  // hide the initial input
			],
			//'allowedFileTypes' => ['image'],
			'dropZoneTitle' => 'Перетащить файлы сюда',
			'ajaxDeleteSettings' => ['method' => 'post'],
			/*'pluginEvents' => [
				'fileuploaded' => "function(event, data, previewId, index) {
					\$.pjax.reload({container:'#files'});
					console.log('filebatchselected');
				}",
				'uploadExtraData' => "function() {
					return {
					'model' : ".$model->tableName().",
					'elem_id' : ".$model->id."
					}
				}",
				'filebatchselected'=> "function() {
					console.log('filebatchselected');
					$('#input-id').on('filebatchselected', function(event, files) {
						console.log('File batch selected triggered')
					}
				}"
			],*/
		]
	]);
} else echo "<p class='not-set'>Картинки можно добавить после сохранения!</p>";
?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
