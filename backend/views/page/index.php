<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'sort',
            'name',
            'alias',
            'description:ntext',
            'short_desc:ntext',
            // 'content:ntext',
            // 'seo_title',
            // 'seo_description:ntext',
            // 'seo_keywords',
            // 'parent',
            // 'active',
            // 'url:url',
            // 'img',
            // 'video',
            // 'bg_head',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
