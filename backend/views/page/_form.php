<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\web\UploadedFile;

/* @var $this yii\web\View */
/* @var $model app\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

	<?php $form = ActiveForm::begin([
			'options' => ['enctype'=>'multipart/form-data']
	]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

		<?= $form->field($model, 'sort')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'parent')->textInput() ?>

    <?= $form->field($model, 'active')->checkBox(['uncheck' => 0, 'selected' => 1]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'img')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'video')->widget(FileInput::classname(), [
        'options' => ['accept' => 'video/*'],
    ]); ?>

    <?= $form->field($model, 'bg_head')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],

    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

	<?php /*
		$imageUrl = self::getUploadedFile(); // get file if available from the db/server

		$image = ($imageUrl == null) ?
			Html::img('/uploads/images/noimg.jpg', ['alt' => 'No image yet']) :
			Html::img($imageUrl, ['alt' => 'img']);

		echo $image;

		if ($imageUrl == null) { // to show FileInput only for new upload
			echo FileInput::widget($options);
		}
		else {
			echo Html::a('Remove Image', $deleteAction, ['class'=>'btn btn-danger']);
		}
	*/?>

</div>
